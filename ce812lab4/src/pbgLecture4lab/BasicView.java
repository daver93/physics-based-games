package pbgLecture4lab;

import java.awt.*;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JComponent;

public class BasicView extends JComponent {
	/* Author: Michael Fairbank
	 * Creation Date: 2016-01-28
	 * Significant changes applied:
	 */
	// background colour
	public static final Color BG_COLOR = Color.BLACK;

	private BasicPhysicsEngine game;
	private Image backgroundImage;
	private String fileName = "src/pbgLecture4lab/stars.jpg";

	public BasicView(BasicPhysicsEngine game) throws IOException {
		this.game = game;
		//backgroundImage = ImageIO.read(new File(fileName));
		backgroundImage = javax.imageio.ImageIO.read(new File(fileName));
	}

	@Override
	public void paintComponent(Graphics g0) {
		BasicPhysicsEngine game;
		synchronized(this) {
			game=this.game;
		}
		Graphics2D g = (Graphics2D) g0;
		// paint the background



		g.setColor(BG_COLOR);
		g.fillRect(0, 0, getWidth(), getHeight());

		g.drawImage(backgroundImage, 0, 0, game.SCREEN_WIDTH, game.SCREEN_HEIGHT,  null);


		for (BasicParticle p : game.particles)
			p.draw(g);
		for (ElasticConnector c : game.connectors)
			c.draw(g);
		for (AnchoredBarrier b : game.barriers)
			b.draw(g);



		g.setColor(Color.RED);
		g.setFont(new Font("TimesRoman", Font.BOLD, 30));

		if (game.isWinner){
			g.drawString("CONGRATS! YOU WON!", 30, 45);
		}
		else {
			if (game.lives == 0){
				g.drawString("GAME OVER!", 30, 45);
			}
			else {
				g.drawString("Score: " + game.lives, 30, 45);
			}
		}
	}

	@Override
	public Dimension getPreferredSize() {
		return BasicPhysicsEngine.FRAME_SIZE;
	}
	
	public synchronized void updateGame(BasicPhysicsEngine game) {
		this.game=game;
	}
}