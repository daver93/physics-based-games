package pbgLecture3lab;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.event.MouseInputAdapter;
import javax.swing.event.MouseInputListener;

public class BasicMouseListener extends MouseInputAdapter implements MouseListener {
	/* Author: Michael Fairbank
	 * Creation Date: 2016-01-28
	 * Significant changes applied:
	 */

	private BasicPhysicsEngine game;

	private static int mouseX, mouseY;
	private static boolean mouseButtonPressed;

	public boolean isMouseDragging;
	public boolean isMouseReleased;

	private Vect2D velocity = new Vect2D(3, 3);

	public BasicMouseListener(BasicPhysicsEngine game){
		this.game = game;
	}

	public void mouseMoved(MouseEvent e) {
       mouseX=e.getX();
       mouseY=e.getY();

       if (isMouseReleased){
			game.hitTheBall();
	   }

       isMouseReleased = false;
       isMouseDragging = false;

    }
	public void mouseDragged(MouseEvent e) {
	   mouseX=e.getX();
	   mouseY=e.getY();
	   isMouseDragging = true;
	   isMouseReleased = true;
	}

	private void test(){
		System.out.println(isMouseDragging);
	}

	public void mouseClicked(MouseEvent e) {
		isMouseDragging = true;
		isMouseReleased = true;
	}

	public void mousePressed(MouseEvent e) {
		//super.mousePressed(e);
		isMouseReleased=false;
		//System.out.println("Pressed " + e.getButton());
	}

	public void mouseReleased(MouseEvent e) {
		if(isMouseReleased) {
			game.hitTheBall();
			isMouseDragging = false;
			isMouseReleased = false;
		}
	}

	public boolean isMouseButtonPressed() {
		//System.out.println("YESSSSS");
		return mouseButtonPressed;
	}

	public boolean isMouseDragged(){
		return isMouseDragging;
	}

	public void setMouseButtonPressed(boolean isPressed){
		mouseButtonPressed = isPressed;
	}

	public Vect2D getWorldCoordinatesOfMousePointer() {
		return new Vect2D(BasicPhysicsEngine.convertScreenXtoWorldX(mouseX), BasicPhysicsEngine.convertScreenYtoWorldY(mouseY));
	}

	public Vect2D getScreenCoodrinatesOfMousePointer(){
		return new Vect2D(mouseX, mouseY);
	}

	public Vect2D fetchVelocity(){
		return velocity;
	}

	public void setFetchVelocity(Vect2D velo){
		this.velocity = velo;
	}

}
