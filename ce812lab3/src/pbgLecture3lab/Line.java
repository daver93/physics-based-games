package pbgLecture3lab;


import java.awt.*;

public class Line {
	/* Author: Michael Fairbank
	 * Creation Date: 2016-01-28
	 * Significant changes applied:
	 */

	private Vect2D startPos,endPos,unitNormal,unitTangent;
	private final Color col;
	private final double barrierLength;
	private final Double barrierDepth;


	public Line(double startx, double starty, double endx, double endy, Color col) {
		this(startx, starty, endx, endy, col, null);
	}

	public Line(double startx, double starty, double endx, double endy, Color col, Double barrierWidth) {
		startPos=new Vect2D(startx,starty);
		endPos=new Vect2D(endx,endy);
		
		Vect2D temp=Vect2D.minus(endPos,startPos);
		this.barrierLength=temp.mag();
		temp=temp.normalise();
		setUnitTangent(temp);
		setUnitNormal(getUnitTangent().rotate90degreesAnticlockwise());
		//this.SCREEN_RADIUS=Math.max(BasicPhysicsEngine.convertWorldLengthToScreenLength(radius),1);
		this.col=col;
		this.barrierDepth=barrierWidth;
	}

	public void draw(Graphics2D g) {
		int x1 = BasicPhysicsEngine.convertWorldXtoScreenX(startPos.x);
		int y1 = BasicPhysicsEngine.convertWorldYtoScreenY(startPos.y);
		int x2 = BasicPhysicsEngine.convertWorldXtoScreenX(endPos.x);
		int y2 = BasicPhysicsEngine.convertWorldYtoScreenY(endPos.y);
		g.setColor(col);
		g.drawLine(x1, y1, x2, y2);
	}

	public Vect2D getUnitNormal() {
		return unitNormal;
	}

	public void setUnitNormal(Vect2D unitNormal) {
		this.unitNormal = unitNormal;
	}

	public Vect2D getUnitTangent() {
		return unitTangent;
	}

	public void setUnitTangent(Vect2D unitTangent) {
		this.unitTangent = unitTangent;
	}

	public void changePosition(double mouseX, double mouseY, double ballX, double ballY){
		startPos=new Vect2D(mouseX,mouseY);
		endPos=new Vect2D(ballX,ballY);
	}

}
