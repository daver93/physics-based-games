package pbgLecture2lab;


import java.awt.Color;
import java.awt.Graphics2D;

public class AnchoredBarrier_Curve extends AnchoredBarrier {

	private final Vect2D centreOfCircleBarrierArc;
	private final Color col;
	private final Double barrierDepth;
	private final double deltaAngle;
	private final double startAngle;
	private final double radiusOfBarrier;
	private final int radiusInScreenCoordinates;
	private final boolean normalPointsInwards;


	public AnchoredBarrier_Curve(double centrex, double centrey, double radiusOfBarrier, double startAngle, double deltaAngle, boolean normalPointsInwards, Color col) {
		this(centrex, centrey,radiusOfBarrier, startAngle, deltaAngle, normalPointsInwards,null, col);
	}

	public AnchoredBarrier_Curve(double centrex, double centrey, double radiusOfBarrier, double startAngle, double deltaAngle, boolean normalPointsInwards, Double barrierDepth, Color col) {
		centreOfCircleBarrierArc=new Vect2D(centrex, centrey);
		this.barrierDepth=barrierDepth;
		this.deltaAngle=deltaAngle;
		this.startAngle=startAngle;
		this.radiusOfBarrier=radiusOfBarrier;
		this.radiusInScreenCoordinates=BasicPhysicsEngine.convertWorldLengthToScreenLength(radiusOfBarrier);
		this.normalPointsInwards=normalPointsInwards;
		this.col=col;
	}

	@Override
	public void draw(Graphics2D g) {
		int x1 = BasicPhysicsEngine.convertWorldXtoScreenX(centreOfCircleBarrierArc.x);
		int y1 = BasicPhysicsEngine.convertWorldYtoScreenY(centreOfCircleBarrierArc.y);
		g.setColor(col);
		// g.drawArc arguments give dimensions of a rectangle (x,y,width,height) that contains the full ellipse that contains the arc
		g.drawArc(x1-radiusInScreenCoordinates, y1-radiusInScreenCoordinates, 
				radiusInScreenCoordinates*2, radiusInScreenCoordinates*2, (int) startAngle, (int) deltaAngle);
	}

	@Override
	public boolean isCircleCollidingBarrier(Vect2D circleCentre, double radius) {
		Vect2D ap=Vect2D.minus(circleCentre, centreOfCircleBarrierArc);
		double ang=ap.angle(); // relies on Math.atan2 function
		ang=ang*180/Math.PI; //convert from radians to degrees
		ang=(ang+360)%360;	// remove any negative angles to avoid confusion
		boolean withinAngleRange=false;
		if (deltaAngle<0 && ((ang>=startAngle+deltaAngle && ang<=startAngle) ||(ang>=startAngle+deltaAngle+360 && ang<=startAngle+360)))
			withinAngleRange=true;
		if (deltaAngle>=0 && ((ang>=startAngle && ang<=startAngle+deltaAngle) ||(ang>=startAngle-360 && ang<=startAngle+deltaAngle-360)))
			withinAngleRange=true;
		double distToCentreOfBarrierArc=ap.mag();
		boolean withinDistanceRange=(normalPointsInwards && distToCentreOfBarrierArc+radius>=this.radiusOfBarrier && distToCentreOfBarrierArc-radius<=this.radiusOfBarrier+(barrierDepth!=null?barrierDepth:0)) 
				|| (!normalPointsInwards && distToCentreOfBarrierArc-radius<=this.radiusOfBarrier && distToCentreOfBarrierArc+radius>=this.radiusOfBarrier-(barrierDepth!=null?barrierDepth:0));
		return withinDistanceRange && withinAngleRange;
	}

	
	@Override
	public Vect2D calculateVelocityAfterACollision(Vect2D pos, Vect2D vel, double e) {

		//at first we must define PC vector (from p to c : p is pos c is the centreOfCircleBarrierArc
		//then we normalise PC to get n vector
		//then rotate n 90 degrees to get t vector (tangent)


		Vect2D pc = Vect2D.minus(pos,centreOfCircleBarrierArc);
		Vect2D n = pc.normalise();
		if (normalPointsInwards){
			// assumes normal points AWAY from wall
			n=n.mult(-1);
			//energy=0.1;
		}


		Vect2D t = new Vect2D(n.y, -n.x);


		// get parallel and perpendicular components
		double vParallel=vel.scalarProduct(t);
		double vNormal=vel.scalarProduct(n);

		// reflect normal component of velocity
		if (vNormal < 0)
			//vNormal=-vNormal;
			vNormal=e*-vNormal;

		// reconstruct the reflected velocity
		Vect2D result=n.mult(vNormal);
		result=result.addScaled(t, vParallel);
		return result;

	}


}
