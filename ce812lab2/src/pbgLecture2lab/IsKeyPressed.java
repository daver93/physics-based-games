package pbgLecture2lab;

import java.awt.*;
import java.awt.event.KeyEvent;

public class IsKeyPressed {

    private static volatile boolean aIsPressed = false;
    private static volatile boolean aIsReleased = false;
    private static volatile boolean dIsPressed = false;
    private static volatile boolean dIsReleased = false;

    public static String isWPressed() {
        synchronized (IsKeyPressed.class) {
            CheckKeyboard();
            return pressedButton();
        }
    }

    public static String pressedButton(){

        if (aIsPressed) return "a";
        else if (aIsReleased) return "aR";
        else if (dIsPressed) return "d";
        else if (dIsReleased) return "dR";

        return "";
    }

    public static void CheckKeyboard() {
        KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(new KeyEventDispatcher() {

            @Override
            public boolean dispatchKeyEvent(KeyEvent ke) {
                synchronized (IsKeyPressed.class) {
                    switch (ke.getID()) {
                        case KeyEvent.KEY_PRESSED:
                            if (ke.getKeyChar() == 'a' && !aIsPressed){
                                aIsPressed = true;
                                aIsReleased = false;

                                dIsPressed = false;
                                dIsPressed = false;
                            }
                            if (ke.getKeyChar() == 'd' && !dIsPressed){
                                dIsPressed = true;
                                dIsReleased = false;

                                aIsPressed = false;
                                aIsReleased = false;
                            }
                            break;

                        case KeyEvent.KEY_RELEASED:
                            if (ke.getKeyChar() == 'a' && aIsPressed){
                                aIsPressed = false;
                                aIsReleased = true;

                                dIsPressed = false;
                                dIsPressed = false;
                            }
                            if (ke.getKeyChar() == 'd' && dIsPressed){
                                dIsPressed = false;
                                dIsReleased = true;

                                aIsPressed = false;
                                aIsReleased = false;
                            }
                            break;
                    }
                    return false;
                }
            }
        });
    }
}