package pbgLecture2lab;

import java.awt.geom.AffineTransform;
import java.lang.Double;
import java.awt.*;

public class AnchoredBarrier_StraightLine extends AnchoredBarrier {

	private Vect2D startPos,endPos,unitNormal,unitTangent;
	private final Color col;
	private final double barrierLength;
	private final Double barrierDepth;

	private final double upPos = 2.0;
	private final double downPos = 1.18;

	private Graphics2D gr;


	public AnchoredBarrier_StraightLine(double startx, double starty, double endx, double endy, Color col) {
		this(startx, starty, endx, endy, col, null);
	}

	public AnchoredBarrier_StraightLine(double startx, double starty, double endx, double endy, Color col, Double barrierWidth) {
		startPos=new Vect2D(startx,starty);
		endPos=new Vect2D(endx,endy);
		
		Vect2D temp=Vect2D.minus(endPos,startPos);
		this.barrierLength=temp.mag();
		temp=temp.normalise();
		setUnitTangent(temp);
		setUnitNormal(getUnitTangent().rotate90degreesAnticlockwise());
		this.col=col;
		this.barrierDepth=barrierWidth;
	}

	@Override
	public void draw(Graphics2D g) {
		int x1 = BasicPhysicsEngine.convertWorldXtoScreenX(startPos.x);
		int y1 = BasicPhysicsEngine.convertWorldYtoScreenY(startPos.y);
		int x2 = BasicPhysicsEngine.convertWorldXtoScreenX(endPos.x);
		int y2 = BasicPhysicsEngine.convertWorldYtoScreenY(endPos.y);

		g.setColor(col);

		//width
		if (barrierDepth != null){
			float depth = barrierDepth.floatValue();
			g.setStroke(new BasicStroke(depth));
		}
		g.drawLine(x1, y1, x2, y2);

		gr = g;
	}

	public void moveBarrierUpwards(String flippersPosition){
		if (endPos.y < upPos){
			if (flippersPosition.equals("Left")) {
				double changeY = endPos.y + 0.1;
				startPos = new Vect2D(startPos.x, startPos.y);
				endPos = new Vect2D(endPos.x, changeY);
			}
			else {
				double changeY = endPos.y + 1;
				startPos = new Vect2D(startPos.x, changeY);
				endPos = new Vect2D(endPos.x, endPos.y);
			}
		}
		change(gr);
	}

	public void moveBarrierDownwards(String flippersPosition){

		if (endPos.y > downPos){
			double changeY = endPos.y - 0.1;

			if (flippersPosition.equals("Left")) {
				startPos = new Vect2D(startPos.x, startPos.y);
				endPos = new Vect2D(endPos.x, changeY);
			}
			else {
				startPos = new Vect2D(startPos.x, changeY);
				endPos = new Vect2D(endPos.x, endPos.y);
			}
		}
		change(gr);
	}


	public void change(Graphics2D g){

		int x1 = BasicPhysicsEngine.convertWorldXtoScreenX(startPos.x);
		int y1 = BasicPhysicsEngine.convertWorldYtoScreenY(startPos.y);
		int x2 = BasicPhysicsEngine.convertWorldXtoScreenX(endPos.x);
		int y2 = BasicPhysicsEngine.convertWorldYtoScreenY(endPos.y);

		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		//g.fillOval(x, y, 30, 30);
		//g.fillRect(startPos.x, endPos, 30, 30);
		g.drawLine(x1, y1, x2, y2);
	}

	@Override	
	public boolean isCircleCollidingBarrier(Vect2D circleCentre, double circleRadius) {
		Vect2D ap=Vect2D.minus(circleCentre, startPos);
		double distOnCorrectSideOfBarrierToCentre=ap.scalarProduct(getUnitNormal());
		double distAlongBarrier=ap.scalarProduct(getUnitTangent());
		// Note barrierDepth is type Double declared in constructor.  
		// barrierDepth null indicates infinite barrierDepth
		// barrierLength is ||AB||, declared in constructor.
		return distOnCorrectSideOfBarrierToCentre<=circleRadius && (barrierDepth==null || distOnCorrectSideOfBarrierToCentre>=-(barrierDepth+circleRadius)) && distAlongBarrier>=0 && distAlongBarrier<=barrierLength;
	}

	@Override
	public Vect2D calculateVelocityAfterACollision(Vect2D pos, Vect2D vel, double e) {
		double vParallel=vel.scalarProduct(getUnitTangent());
		double vNormal=vel.scalarProduct(getUnitNormal());
		if (vNormal<0) // assumes normal points AWAY from wall...
			vNormal=-vNormal;
		Vect2D result=getUnitTangent().mult(vParallel);
		result=result.addScaled(getUnitNormal(), vNormal);
		return result;
	}

	public Vect2D getUnitNormal() {
		return unitNormal;
	}

	public void setUnitNormal(Vect2D unitNormal) {
		this.unitNormal = unitNormal;
	}

	public Vect2D getUnitTangent() {
		return unitTangent;
	}

	public void setUnitTangent(Vect2D unitTangent) {
		this.unitTangent = unitTangent;
	}

}
