package pbgLecture5lab_wrapperForJBox2D;

import java.awt.*;

import javax.swing.JComponent;
import org.jbox2d.common.Vec2;

public class BasicView extends JComponent {
	/* Author: Michael Fairbank
	 * Creation Date: 2016-01-28
	 * Significant changes applied:
	 */
	// background colour
	public static final Color BG_COLOR = Color.BLACK;

	private BasicPhysicsEngineUsingBox2D game;

	public BasicView(BasicPhysicsEngineUsingBox2D game) {
		this.game = game;
	}
	
	@Override
	public void paintComponent(Graphics g0) {
		BasicPhysicsEngineUsingBox2D game;
		synchronized(this) {
			game=this.game;
		}
		Graphics2D g = (Graphics2D) g0;
		// paint the background
		g.setColor(BG_COLOR);
		g.fillRect(0, 0, getWidth(), getHeight());
		
		for (BasicParticle p : game.particles)
			p.draw(g);
		for (BasicPolygon p : game.polygons)
			p.draw(g);		
		for (ElasticConnector c : game.connectors)
			c.draw(g);
		for (AnchoredBarrier b : game.barriers)
			b.draw(g);

		game.cannon.draw(g);

		game.flyingFloor.draw(g);

		DrawMouseLine(g);

		if (game.isWinner){
			g.setColor(Color.RED);
			g.setFont(new Font("TimesRoman", Font.BOLD, 30));
			g.drawString("Gongrats! You are the winner! ", game.SCREEN_WIDTH/5, game.SCREEN_HEIGHT/2);
		}
		else {
			g.setColor(Color.RED);
			g.setFont(new Font("TimesRoman", Font.BOLD, 30));
			g.drawString("Try No: " + game.numberOfTries, 30, 45);
		}
	}

	@Override
	public Dimension getPreferredSize() {
		return BasicPhysicsEngineUsingBox2D.FRAME_SIZE;
	}
	
	public synchronized void updateGame(BasicPhysicsEngineUsingBox2D game) {
		this.game=game;
	}


	// Draw direction pointer line
	public void DrawMouseLine(Graphics2D g){
		Vec2 start=game.basicMouseListener.getWorldCoordinatesOfMousePointer();
		Vec2 end = game.cannon.body.getPosition();
		g.setColor(new Color(101, 67, 33));
		int x1 = BasicPhysicsEngineUsingBox2D.convertWorldXtoScreenX(start.x);
		int y1 = BasicPhysicsEngineUsingBox2D.convertWorldYtoScreenY(start.y);
		int x2 = BasicPhysicsEngineUsingBox2D.convertWorldXtoScreenX(end.x);
		int y2 = BasicPhysicsEngineUsingBox2D.convertWorldYtoScreenY(end.y);
		//g.drawLine(x1,y1,x2,y2);
	}
}