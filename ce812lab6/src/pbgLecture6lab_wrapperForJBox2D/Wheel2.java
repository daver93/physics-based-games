package pbgLecture6lab_wrapperForJBox2D;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Path2D;
import java.awt.geom.PathIterator;

import org.jbox2d.dynamics.*;
import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.BodyType;
import org.jbox2d.dynamics.FixtureDef;
import org.jbox2d.dynamics.World;

import org.jbox2d.collision.shapes.CircleShape;

public class Wheel2 {

    public final float ratioOfScreenScaleToWorldScale;

    public final int SCREEN_RADIUS;

    private final float rollingFriction,mass;
    public final Color col;
    protected final Body body;
    public final float radius;

    public CircleShape circleShape;
    public Wheel2(float initialPosX, float initialPosY, float initialVelX, float initialVelY, float radius, Color col, float mass, float rollingFriction){

        this.ratioOfScreenScaleToWorldScale=BasicPhysicsEngineUsingBox2D.convertWorldLengthToScreenLength(1);
        this.col=col;
        this.rollingFriction=rollingFriction;
        this.mass=mass;
        this.SCREEN_RADIUS=(int)Math.max(BasicPhysicsEngineUsingBox2D.convertWorldLengthToScreenLength(radius),1);
        this.radius = radius;

        World world = BasicPhysicsEngineUsingBox2D.world;

        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyType.DYNAMIC;
        bodyDef.position.set(initialPosX/2, initialPosY/2);
        bodyDef.linearVelocity.set(initialVelX, initialVelY);
        bodyDef.angularVelocity = 0f;
        bodyDef.angularDamping=0.1f;
        this.body = world.createBody(bodyDef);

        circleShape = new CircleShape();
        circleShape.m_radius = radius;

        FixtureDef fixtureDef = new FixtureDef();// This class is from Box2D
        fixtureDef.shape = circleShape;
        fixtureDef.density = (float) (mass/(Math.PI*radius*radius));
        fixtureDef.friction = 0.2f;// this is surface friction;
        fixtureDef.restitution = 1.0f;

        body.createFixture(fixtureDef);
    }

    public static Ellipse2D.Float mkRegularEllipse(float width, float height, float upperLeftX, float upperLeftY){
        Ellipse2D.Float e = new Ellipse2D.Float();

        e.width = width;
        e.height = height;
        e.x = upperLeftX;
        e.y = upperLeftY;

        return e;
    }

    public void draw(Graphics2D g) {
        int x = BasicPhysicsEngineUsingBox2D.convertWorldXtoScreenX(body.getWorldCenter().x);
        int y = BasicPhysicsEngineUsingBox2D.convertWorldYtoScreenY(body.getWorldCenter().y);

        g.setColor(col);

        g.fillOval(x - SCREEN_RADIUS, y - SCREEN_RADIUS, 2 * SCREEN_RADIUS, 2 * SCREEN_RADIUS);

        g.setColor(Color.BLACK);
    }

    public void notificationOfNewTimestep() {
        if (rollingFriction>0) {
            Vec2 rollingFrictionForce=new Vec2(body.getLinearVelocity());
            rollingFrictionForce=rollingFrictionForce.mul(-rollingFriction*mass);
            body.applyForceToCenter(rollingFrictionForce);
        }
    }
}
