package pbgLecture6lab_wrapperForJBox2D;

import org.jbox2d.common.Timer;

import javax.swing.*;
import java.awt.*;

public class BasicView extends JComponent {

	public static final Color BG_COLOR = Color.BLACK;

	private BasicPhysicsEngineUsingBox2D game;

	private int currentTimePassed;
	private int finalTime;

	private Timer timer;

	public BasicView(BasicPhysicsEngineUsingBox2D game) {
		this.game = game;

		timer = new Timer();
		currentTimePassed = 0;
		finalTime = currentTimePassed;
	}
	
	@Override
	public void paintComponent(Graphics g0) {
		BasicPhysicsEngineUsingBox2D game;
		synchronized(this) {
			game=this.game;
		}
		Graphics2D g = (Graphics2D) g0;
		// paint the background
		g.setColor(BG_COLOR);
		g.fillRect(0, 0, getWidth(), getHeight());
		
		for (BasicParticle p : game.particles)
			p.draw(g);
		for (BasicPolygon p : game.polygons)
			p.draw(g);		
		//for (ElasticConnector c : game.connectors)
		//	c.draw(g);
		for (AnchoredBarrier b : game.barriers)
			b.draw(g);

		if (BasicPhysicsEngineUsingBox2D.cart != null){
			BasicPhysicsEngineUsingBox2D.cart.draw(g);
		}
		if (game.car001.pole != null) {
			game.car001.pole.draw(g);
		}

		if (game.car001 != null && !game.car001.wheels.isEmpty()) {
			for (Wheel2 wheel : game.car001.wheels){
				if (wheel == game.car001.wheels.get(0)){
					wheel.draw(g);
				}
				else {
					wheel.draw(g);
				}
			}
		}

		currentTimePassed = (int) (timer.getMilliseconds() * 0.001);

		if (game.car001.pole.gameover){
			g.setColor(Color.RED);
			g.setFont(new Font("TimesRoman", Font.BOLD, 30));
			if (finalTime == 0){
				finalTime = currentTimePassed;
			}
			g.drawString("GAME OVER! Total Time: " + finalTime, 30, 45);
		}
		else {
			g.setColor(Color.RED);
			g.setFont(new Font("TimesRoman", Font.BOLD, 30));
			g.drawString("Time: " + currentTimePassed, 30, 45);
		}
	}

	@Override
	public Dimension getPreferredSize() {
		return BasicPhysicsEngineUsingBox2D.FRAME_SIZE;
	}
	
	public synchronized void updateGame(BasicPhysicsEngineUsingBox2D game) {
		this.game=game;
	}
}