package pbgLecture6lab_wrapperForJBox2D;

import java.awt.Color;
import java.awt.Graphics2D;

import org.jbox2d.dynamics.*;
import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.BodyType;
import org.jbox2d.dynamics.FixtureDef;
import org.jbox2d.dynamics.World;

public class Cart1 {

	public final float ratioOfScreenScaleToWorldScale;

	private final float rollingFriction,mass;
	public final Color col;
	protected final Body body;

	public float width;
	public float height;

	public Cart1(float sx, float sy, float vx, float vy, Color col, float mass, float rollingFriction, float width, float height, int numSides) {

		this.width = width;
		this.height = height;

		this.ratioOfScreenScaleToWorldScale=BasicPhysicsEngineUsingBox2D.convertWorldLengthToScreenLength(1);
		this.col=col;
		this.rollingFriction=rollingFriction;
		this.mass=mass;

		World w=BasicPhysicsEngineUsingBox2D.world; // a Box2D object

		BodyDef bodyDef = new BodyDef();  // a Box2D object
		bodyDef.type = BodyType.DYNAMIC;

		bodyDef.position.set(sx/2, sy/2);
		bodyDef.setAngularVelocity(0);

		bodyDef.linearVelocity.set(vx, vy);
		bodyDef.angularDamping = 0.1f;
		this.body = w.createBody(bodyDef);

		PolygonShape shape = new PolygonShape();
		shape.setAsBox(width/2, height/2);

		FixtureDef fixtureDef = new FixtureDef();// This class is from Box2D
		fixtureDef.shape = shape;
		fixtureDef.density = 0f;
		fixtureDef.friction = 0.1f;// this is surface friction;
		fixtureDef.restitution = 0.5f;
		body.createFixture(fixtureDef);
	}

	public void draw(Graphics2D g) {
		g.setColor(col);

		int x = BasicPhysicsEngineUsingBox2D.convertWorldXtoScreenX(body.getPosition().x-width/2);
		int y = BasicPhysicsEngineUsingBox2D.convertWorldYtoScreenY(body.getPosition().y+height/2);

		int w = (int) BasicPhysicsEngineUsingBox2D.convertWorldLengthToScreenLength(width);
		int h = (int) BasicPhysicsEngineUsingBox2D.convertWorldLengthToScreenLength(height);

		g.fillRect(x, y, w, h);
	}

	public void notificationOfNewTimestep() {
		if (rollingFriction>0) {
			Vec2 rollingFrictionForce=new Vec2(body.getLinearVelocity());
			rollingFrictionForce=rollingFrictionForce.mul(-rollingFriction*mass);
			body.applyForceToCenter(rollingFrictionForce);
		}
	}
}
