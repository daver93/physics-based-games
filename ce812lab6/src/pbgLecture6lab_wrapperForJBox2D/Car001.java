package pbgLecture6lab_wrapperForJBox2D;

import java.awt.*;
import java.util.ArrayList;

import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.joints.RevoluteJointDef;
import org.jbox2d.common.MathUtils;

public class Car001 {

    Wheel2 wheel1;
    Wheel2 wheel2;

    Pole pole;

    Cart1 cart;

    ArrayList<Wheel2> wheels;

    public Car001(float cartX, float cartY) {

        cart = BasicPhysicsEngineUsingBox2D.cart;

        pole = new Pole(cartX+cart.width,cartY+cart.height*2,0,0, Color.RED, 5, 0.0f, 0.1f, 3);

        wheel1 = new Wheel2(cartX+cart.width-3, cartY-cart.height+1, 0, 0, 0.5f, Color.YELLOW, 10, 4.5f);
        wheel2 = new Wheel2(cartX+cart.width+2, cartY-cart.height+1, 0, 0, 0.5f, Color.YELLOW, 10, 4.5f);

        wheels = new ArrayList<Wheel2>();
        wheels.add(wheel1);
        wheels.add(wheel2);

        createPoleJoint();
        createWheelsJoint();
    }

    public void createWheelsJoint(){
        // Define joints
        for (Wheel2 wheel : wheels){
            RevoluteJointDef rjd1 = new RevoluteJointDef();

            rjd1.initialize(cart.body, wheel.body, wheel.body.getWorldCenter());

            rjd1.motorSpeed = -MathUtils.PI*2;
            //rjd1.maxMotorTorque = 300.0f;
            rjd1.enableMotor = true;
            rjd1.collideConnected=false;
            BasicPhysicsEngineUsingBox2D.world.createJoint(rjd1);
        }
    }

    public void createPoleJoint(){
        RevoluteJointDef jointDef = new RevoluteJointDef();

        Vec2 tempPos = pole.body.getPosition().sub(new Vec2(0, 2));

        //jointDef.initialize(cart.body, pole.body, tempPos);
        jointDef.bodyA = cart.body;
        jointDef.bodyB = pole.body;

        jointDef.collideConnected = false;
        jointDef.enableMotor = false; //is it on?
        //jointDef.motorSpeed = -MathUtils.PI*2; //how fast
        //jointDef.maxMotorTorque = 300f; //how powerful

        jointDef.localAnchorA=new Vec2(0.0f,0.0f); // the coordinates of the pivot in bodyA
        jointDef.localAnchorB=new Vec2(0, (-pole.body.getPosition().y/2f )); // the coordinates of the pivot in bodyB

        BasicPhysicsEngineUsingBox2D.world.createJoint(jointDef);
    }
}
