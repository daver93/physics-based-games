package pbgLecture6lab_wrapperForJBox2D;

import java.awt.*;
import java.util.ArrayList;

import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.joints.RevoluteJointDef;
import org.jbox2d.common.MathUtils;

public class Car001 {

    Wheel2 wheel1;
    Wheel2 wheel2;

    Pole pole;

    Cart1 cart;

    ArrayList<Wheel2> wheels;

    public Car001(float cartX, float cartY) {

        cart = BasicPhysicsEngineUsingBox2D.cart;

        wheel1 = new Wheel2(cartX+cart.width+0.5f, cartY-cart.height+1, 0, 0, 0.3f, Color.YELLOW, 2, 4.5f);
        wheel2 = new Wheel2(cartX+cart.width+3.5f, cartY-cart.height+1, 0, 0, 0.3f, Color.YELLOW, 2, 4.5f);

        wheels = new ArrayList<Wheel2>();
        wheels.add(wheel1);
        wheels.add(wheel2);

        createWheelsJoint();
    }

    public void moveWheel(float acceleration){
        if (acceleration != wheel1.body.m_torque){
            wheel1.body.applyTorque(acceleration);

            //cart.body.applyForceToCenter(new Vec2(10, 5));
            cart.body.applyForceToCenter(new Vec2(-acceleration/2, 0));
            cart.body.applyTorque(-acceleration);
            //cart.body.setLinearVelocity(new Vec2(5, 0));
        }
    }

    public void createWheelsJoint(){
        // Define joints
        for (Wheel2 wheel : wheels){
            RevoluteJointDef rjd1 = new RevoluteJointDef();

            rjd1.initialize(cart.body, wheel.body, wheel.body.getPosition());

            rjd1.enableMotor = false;
            rjd1.collideConnected=false;
            //rjd1.maxMotorTorque = 300.0f;
            //rjd1.motorSpeed = -MathUtils.PI*2;

            BasicPhysicsEngineUsingBox2D.world.createJoint(rjd1);
        }
    }

    public void createPoleJoint(){
        RevoluteJointDef jointDef = new RevoluteJointDef();

        Vec2 tempPos = pole.body.getPosition().sub(new Vec2(0, 2));

        jointDef.initialize(cart.body, pole.body, tempPos);
        jointDef.bodyA = cart.body;
        jointDef.bodyB = pole.body;

        jointDef.collideConnected = false;
        jointDef.enableMotor = false; //is it on?
        //jointDef.motorSpeed = -MathUtils.PI*2; //how fast
        //jointDef.maxMotorTorque = 300f; //how powerful

        jointDef.localAnchorA=new Vec2(0.0f,0.0f); // the coordinates of the pivot in bodyA
        jointDef.localAnchorB=new Vec2(0, (-pole.body.getPosition().y/2f )); // the coordinates of the pivot in bodyB

        BasicPhysicsEngineUsingBox2D.world.createJoint(jointDef);
    }
}
