package pbgLecture6lab_wrapperForJBox2D;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Path2D;

import org.jbox2d.dynamics.*;
import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.BodyType;
import org.jbox2d.dynamics.FixtureDef;
import org.jbox2d.dynamics.World;

public class Cart1 {

	public final float ratioOfScreenScaleToWorldScale;

	private final float rollingFriction,mass;
	public final Color col;
	protected final Body body;

	public static float width;
	public static float height;

	private Vec2[] vertices;

	public Cart1(float sx, float sy, float vx, float vy, Color col, float mass, float rollingFriction, float width, float height, int numSides) {

		this.width = width;
		this.height = height;

		this.ratioOfScreenScaleToWorldScale=BasicPhysicsEngineUsingBox2D.convertWorldLengthToScreenLength(1);
		this.col=col;
		this.rollingFriction=rollingFriction;
		this.mass=mass;

		polygonPath = mkRegularRectangle(width, height);

		World w=BasicPhysicsEngineUsingBox2D.world; // a Box2D object

		BodyDef bodyDef = new BodyDef();  // a Box2D object
		bodyDef.type = BodyType.DYNAMIC;

		bodyDef.position.set(sx/2, sy/2);
		bodyDef.setAngularVelocity(0);

		bodyDef.linearVelocity.set(vx, vy);
		bodyDef.linearDamping = 0.01f;
		bodyDef.angularDamping = 0.1f;
		this.body = w.createBody(bodyDef);

		PolygonShape shape = new PolygonShape();
		vertices = verticesOfPath2D(polygonPath, 4);
		shape.set(vertices, 4);
		//shape.setAsBox(width/2, height/2);

		FixtureDef fixtureDef = new FixtureDef();// This class is from Box2D
		fixtureDef.shape = shape;
		fixtureDef.density = 0.1f;
		fixtureDef.friction = 0.0f;// this is surface friction;
		fixtureDef.restitution = 0.5f;
		body.createFixture(fixtureDef);
	}

	// Vec2 vertices of Path2D
	public static Vec2[] verticesOfPath2D(Path2D.Float p, int n) {

		Vec2[] vertices = new Vec2[4];

		vertices[0]=new Vec2(-width/2, -height/2);
		vertices[1]=new Vec2(width/2, -height/2);
		vertices[2]=new Vec2(width/2, height/2);
		vertices[3]=new Vec2(-width/2, height/2);

		return vertices;
	}

	private final Path2D.Float polygonPath;
	public void draw(Graphics2D g) {
		g.setColor(Color.GREEN);
		Vec2 position = body.getPosition();
		float angle = body.getAngle();
		AffineTransform af = new AffineTransform();
		af.translate(BasicPhysicsEngineUsingBox2D.convertWorldXtoScreenX(position.x), BasicPhysicsEngineUsingBox2D.convertWorldYtoScreenY(position.y));
		af.scale(ratioOfScreenScaleToWorldScale, -ratioOfScreenScaleToWorldScale);// there is a minus in here because screenworld is flipped upsidedown compared to physics world
		af.rotate(angle);
		Path2D.Float p = new Path2D.Float (polygonPath,af);
		g.fill(p);

	}

	public static Path2D.Float mkRegularRectangle(float width, float height) {

		Path2D.Float p = new Path2D.Float();
		float x;
		float y;

		p.moveTo(width/2, height/2);

		x = width/2;
		y = height/2;
		p.lineTo(x, y);

		x = -width/2;
		y = height/2;
		p.lineTo(x, y);

		x = -width/2;
		y = -height/2;
		p.lineTo(x, y);

		x = width/2;
		y = -height/2;
		p.lineTo(x, y);

		p.closePath();
		return p;
	}

	public void notificationOfNewTimestep() {
		if (rollingFriction>0) {
			Vec2 rollingFrictionForce=new Vec2(body.getLinearVelocity());
			rollingFrictionForce=rollingFrictionForce.mul(-rollingFriction*mass);
			body.applyForceToCenter(rollingFrictionForce);
		}
	}
}
