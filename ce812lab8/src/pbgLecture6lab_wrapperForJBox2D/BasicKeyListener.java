package pbgLecture6lab_wrapperForJBox2D;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class BasicKeyListener extends KeyAdapter {
	/* Author: Michael Fairbank
	 * Creation Date: 2016-01-28
	 * Significant changes applied:
	 */
	private static boolean goRightKeyPressed, goLeftKeyPressed, thrustKeyPressed, landingKeyPressed, spacebarKeyPressed;

	public static boolean goRightKeyPressed() {
		return goRightKeyPressed;
	}

	public static boolean goLeftKeyPressed() {
		return goLeftKeyPressed;
	}

	public static boolean isThrustKeyPressed() {
		return thrustKeyPressed;
	}

	public static boolean isLandingKeyPressed() {
		return landingKeyPressed;
	}

	public static boolean isSpacebarKeyPressed() {
		return spacebarKeyPressed;
	}

	public void keyPressed(KeyEvent e) {
		int key = e.getKeyCode();
		switch (key) {
		case KeyEvent.VK_UP:
			thrustKeyPressed=true;
			break;
		case KeyEvent.VK_LEFT:
			goLeftKeyPressed=true;
			break;
		case KeyEvent.VK_RIGHT:
			goRightKeyPressed=true;
			break;
		case KeyEvent.VK_DOWN:
			landingKeyPressed = true;
			break;
		case KeyEvent.VK_SPACE:
			spacebarKeyPressed = true;
			break;
		}
	}

	public void keyReleased(KeyEvent e) {
		int key = e.getKeyCode();
		switch (key) {
		case KeyEvent.VK_UP:
			thrustKeyPressed=false;
			break;
		case KeyEvent.VK_LEFT:
			goLeftKeyPressed=false;
			break;
		case KeyEvent.VK_RIGHT:
			goRightKeyPressed=false;
			break;
		case KeyEvent.VK_DOWN:
			landingKeyPressed = false;
			break;
		case KeyEvent.VK_SPACE:
			spacebarKeyPressed = false;
			break;
		}
	}
}
