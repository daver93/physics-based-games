package mainGame;

import org.jbox2d.dynamics.BodyType;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Path2D;
import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.dynamics.FixtureDef;

/*
 * Author David Triantafyllou
 */

public class BasicRectangle {

    public final float ratioOfScreenScaleToWorldScale;

    protected final float rollingFriction,mass;
    public Color col;

    public static float width;
    public static float height;

    protected org.jbox2d.common.Vec2[] vertices;

    protected org.jbox2d.dynamics.Body body;
    protected org.jbox2d.dynamics.World w;
    protected org.jbox2d.dynamics.BodyDef bodyDef;
    protected org.jbox2d.collision.shapes.PolygonShape shape;
    protected org.jbox2d.dynamics.FixtureDef fixtureDef;


    public BasicRectangle(float sx, float sy, float vx, float vy, Color col, float mass, float rollingFriction, float width, float height) {

        this.width = width;
        this.height = height;

        this.ratioOfScreenScaleToWorldScale= MainGameEngine.convertWorldLengthToScreenLength(1);
        this.col=col;
        this.rollingFriction=rollingFriction;
        this.mass=mass;

        polygonPath = mkRegularRectangle(width, height);

        w= MainGameEngine.world; // a Box2D object

        bodyDef = new org.jbox2d.dynamics.BodyDef();  // a Box2D object
        bodyDef.type = BodyType.KINEMATIC;
        bodyDef.position.set(sx/2, sy/2);

        bodyDef.setAngularVelocity(0);
        bodyDef.angularDamping = 1f;

        bodyDef.linearVelocity.set(vx, vy);
        //bodyDef.linearDamping = 0.01f;

        this.body = w.createBody(bodyDef);

        shape = new PolygonShape();
        vertices = verticesOfPath2D(polygonPath, 4);
        shape.set(vertices, 4);

        fixtureDef = new FixtureDef();// This class is from Box2D
        fixtureDef.shape = shape;
        fixtureDef.density = 2 * width * height * mass;
        fixtureDef.friction = 0.0f;// this is surface friction;
        fixtureDef.restitution = 0.5f;

        body.createFixture(fixtureDef);
    }

    public static org.jbox2d.common.Vec2[] verticesOfPath2D(Path2D.Float p, int n) {

        org.jbox2d.common.Vec2[] vertices = new org.jbox2d.common.Vec2[4];

        vertices[0]=new org.jbox2d.common.Vec2(-width/2, -height/2);
        vertices[1]=new org.jbox2d.common.Vec2(width/2, -height/2);
        vertices[2]=new org.jbox2d.common.Vec2(width/2, height/2);
        vertices[3]=new org.jbox2d.common.Vec2(-width/2, height/2);

        return vertices;
    }

    protected final Path2D.Float polygonPath;

    public void draw(Graphics2D g) {
        g.setColor(col);
        org.jbox2d.common.Vec2 position = body.getPosition();
        float angle = body.getAngle();

        AffineTransform af = new AffineTransform();
        af.translate(MainGameEngine.convertWorldXtoScreenX(position.x), MainGameEngine.convertWorldYtoScreenY(position.y));
        af.scale(ratioOfScreenScaleToWorldScale, -ratioOfScreenScaleToWorldScale);// there is a minus in here because screenworld is flipped upsidedown compared to physics world
        af.rotate(angle);

        Path2D.Float p = new Path2D.Float (polygonPath,af);
        g.fill(p);

    }

    public static Path2D.Float mkRegularRectangle(float width, float height) {

        Path2D.Float p = new Path2D.Float();
        float x;
        float y;

        p.moveTo(width/2, height/2);

        x = -width/2;
        y = height/2;
        p.lineTo(x, y);

        x = -width/2;
        y = -height/2;
        p.lineTo(x, y);

        x = width/2;
        y = -height/2;
        p.lineTo(x, y);

        x = width/2;
        y = height/2;
        p.lineTo(x, y);

        p.closePath();
        return p;
    }

    public void notificationOfNewTimestep() {
        if (rollingFriction>0) {
            org.jbox2d.common.Vec2 rollingFrictionForce=new org.jbox2d.common.Vec2(body.getLinearVelocity());
            rollingFrictionForce=rollingFrictionForce.mul(-rollingFriction*mass);
            body.applyForceToCenter(rollingFrictionForce);
        }
    }
}
