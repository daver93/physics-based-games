package mainGame;

import java.awt.Color;
import java.awt.Graphics2D;

import org.jbox2d.collision.shapes.CircleShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.BodyType;
import org.jbox2d.dynamics.FixtureDef;
import org.jbox2d.dynamics.World;

public class BasicParticle  {
	/* Author: Michael Fairbank
	 * Creation Date: 2016-02-05 (JBox2d version)
	 * Significant changes applied:
	 */
	public final int SCREEN_RADIUS;

	private final float rollingFriction,mass;
	public final Color col;
	protected final Body body;

	private float radius;

	public BasicParticle(float sx, float sy, float vx, float vy, float radius, Color col, float mass, float rollingFriction, Object userData) {
		World w= MainGameEngine.world; // a Box2D object
		BodyDef bodyDef = new BodyDef();  // a Box2D object
		bodyDef.type = BodyType.DYNAMIC; // this says the physics engine is to move it automatically
		bodyDef.position.set(sx, sy);
		bodyDef.linearVelocity.set(vx, vy);
		if (userData != null){
			bodyDef.setUserData(userData);
		}

		this.body = w.createBody(bodyDef);

		CircleShape circleShape = new CircleShape();// This class is from Box2D
		circleShape.m_radius = radius;

		FixtureDef fixtureDef = new FixtureDef();// This class is from Box2D
		fixtureDef.shape = circleShape;
		fixtureDef.density = (float) (mass/(Math.PI*radius*radius));
		fixtureDef.friction = 0.0f;// this is surface friction;
		fixtureDef.restitution = 1.0f;

		body.createFixture(fixtureDef);

		this.rollingFriction=rollingFriction;
		this.mass=mass;
		this.SCREEN_RADIUS=(int)Math.max(MainGameEngine.convertWorldLengthToScreenLength(radius),1);
		this.col=col;

		this.radius = radius;
	}

	public BasicParticle(float sx, float sy, float vx, float vy, float radius, Color col, float mass, float rollingFriction) {
		this (sx, sy, vx, vy, radius, col, mass, rollingFriction, null);
	}

	public void draw(Graphics2D g) {
		int x = MainGameEngine.convertWorldXtoScreenX(body.getPosition().x);
		int y = MainGameEngine.convertWorldYtoScreenY(body.getPosition().y);
		g.setColor(col);
		g.fillOval(x - SCREEN_RADIUS, y - SCREEN_RADIUS, 2 * SCREEN_RADIUS, 2 * SCREEN_RADIUS);
	}


	public void notificationOfNewTimestep() {
		if (rollingFriction>0) {
			Vec2 rollingFrictionForce=new Vec2(body.getLinearVelocity());
			rollingFrictionForce=rollingFrictionForce.mul(-rollingFriction*mass);
			body.applyForceToCenter(rollingFrictionForce);
			
		}
	}

	public float getParticleRadius(){
		return this.radius;
	}
	
}
