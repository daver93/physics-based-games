package mainGame;

import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.BodyType;

import java.awt.*;


public class ShootingCannon extends BasicRectangle{

	/*
	 * Author David Triantafyllou
	 */

	public ShootingCannon(float sx, float sy, float vx, float vy, Color col, float mass, float rollingFriction, float width, float height){
		super(sx, sy, vx, vy, col, mass, rollingFriction, width, height);

		w.destroyBody(body);
		bodyDef.type = BodyType.DYNAMIC;
		this.body = w.createBody(bodyDef);
	}

	@Override
	public void draw(Graphics2D g){
		col = Color.CYAN;
		super.draw(g);
	}

	public void notificationOfNewTimestep() {
		if (rollingFriction>0) {
			Vec2 rollingFrictionForce=new Vec2(body.getLinearVelocity());
			rollingFrictionForce=rollingFrictionForce.mul(-rollingFriction*mass);
			body.applyForceToCenter(rollingFrictionForce);
		}
	}
	
}
