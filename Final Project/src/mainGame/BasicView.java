package mainGame;

import java.awt.*;

import javax.swing.JComponent;

import org.jbox2d.dynamics.Body;

public class BasicView extends JComponent {
	// Author David Triantafyllou

	// background colour
	public static final Color BG_COLOR = Color.BLACK;

	private MainGameEngine game;

	private boolean isRopeInitialized;
	private int ropeAPositionX, ropeAPositionY, ropeBPositionX, ropeBPositionY;

	public boolean showSplashScreen;

	public BasicView(MainGameEngine game) {
		this.game = game;
		isRopeInitialized = false;
		showSplashScreen = true;
	}
	
	@Override
	public void paintComponent(Graphics graphics) {
		MainGameEngine game;
		synchronized(this) {
			game=this.game;
		}
		Graphics2D g = (Graphics2D) graphics;
		// paint the background
		g.setColor(BG_COLOR);
		g.fillRect(0, 0, getWidth(), getHeight());

		if (showSplashScreen){
			drawSplashScreen(g);
		}
		else {
			for (BasicParticle p : game.particles)
				p.draw(g);
			for (BasicPolygon p : game.targets)
				p.draw(g);

			game.canonBase01.draw(g);
			game.shootingCannon.draw(g);

			for (LevelObjects p : game.levelObjects){
				p.draw(g);
			}

			for (BasicRectangle f : game.floors){
				f.draw(g);
			}

			for (LevelObjects w : game.walls){
				w.draw(g);
			}

			for (LevelObjects b : game.borders){
				b.draw(g);
			}

			for (LevelObjects w : game.platforms){
				w.draw(g);
			}

			if (game.level == 7){
				drawRopesOfPulley(g);
			}

			drawInformationTexts(g);
		}
	}

	public void drawInformationTexts(Graphics2D g){
		if (game.isWinner){
			g.setColor(Color.RED);
			g.setFont(new Font("TimesRoman", Font.BOLD, 30));
			g.drawString("Gongrats! You are the winner! ", game.SCREEN_WIDTH/5, game.SCREEN_HEIGHT/2);
		}
		else {
			g.setColor(Color.RED);
			g.setFont(new Font("TimesRoman", Font.BOLD, 30));

			//First draw on the left side of the window the number of tries (how many balls are thrown)
			g.drawString("Try No: " + game.numberOfTries, 30, 45);

			//Then draw at the right side of the window information of the current level
			g.drawString("Level: " + game.level, game.SCREEN_WIDTH - 150, 45);

			//Then draw at the middle the overall elapsed time (during the entire game)
			g.drawString("Time: " + String.format("%.2f", game.time), game.SCREEN_WIDTH/2-100, 45);
		}
	}

	//In this method the ropes for the pulley are drawn
	public void drawRopesOfPulley(Graphics2D g){
	    // Get the two platforms and the wall that they will be connected
		Body bodyA = game.platforms.get(0).body;
		Body bodyB = game.platforms.get(1).body;
		Body bodyC = game.walls.get(0).body;

		if (!isRopeInitialized){
		    // Get the position of each rope .. In the x it should be in the middle of the connected platform
			ropeAPositionX = MainGameEngine.convertWorldXtoScreenX(bodyA.getWorldCenter().x);
			ropeAPositionY = MainGameEngine.convertWorldYtoScreenY(bodyC.getWorldCenter().y);

			ropeBPositionX = MainGameEngine.convertWorldXtoScreenX(bodyB.getWorldCenter().x);
			ropeBPositionY = MainGameEngine.convertWorldYtoScreenY(bodyC.getWorldCenter().y);

			isRopeInitialized = true;
		}

		// Draw the ropes. Actually they are just 2 lines. The rope should not collide with anything, for that reason the drawLine is a good choice
		g.drawLine(MainGameEngine.convertWorldXtoScreenX(bodyA.getWorldCenter().x), MainGameEngine.convertWorldYtoScreenY(bodyA.getWorldCenter().y), ropeAPositionX, ropeAPositionY);
		g.drawLine(MainGameEngine.convertWorldXtoScreenX(bodyB.getWorldCenter().x), MainGameEngine.convertWorldYtoScreenY(bodyB.getWorldCenter().y), ropeBPositionX, ropeBPositionY);
	}

	public void drawSplashScreen(Graphics2D graphics2D){
		graphics2D.setColor(new Color(0xFF0500));
		graphics2D.setFont(new Font("TimesRoman", Font.BOLD, 30));

		//First draw on the left side of the window the number of tries (how many balls are thrown)
		graphics2D.drawString("Drop Them Down", game.SCREEN_WIDTH/2-130, 45);

		graphics2D.drawString("By: David Triantafyllou", game.SCREEN_WIDTH/2-180, 120);

		graphics2D.drawString("How To Play", game.SCREEN_WIDTH/2-100, game.SCREEN_HEIGHT/2 - 100);
		graphics2D.setFont(new Font("TimesRoman", Font.PLAIN, 30));
		graphics2D.drawString("Press UP/DOWN arrows to rotate", game.SCREEN_WIDTH/2-250, game.SCREEN_HEIGHT/2 - 50);
		graphics2D.drawString("Press Spacebar to shoot a ball", game.SCREEN_WIDTH/2-230, game.SCREEN_HEIGHT/2 - 0);

		graphics2D.setFont(new Font("TimesRoman", Font.BOLD, 30));
		graphics2D.drawString("Goal", game.SCREEN_WIDTH/2-50, game.SCREEN_HEIGHT/2 + 150);
		graphics2D.setFont(new Font("TimesRoman", Font.PLAIN, 30));
		graphics2D.drawString("Drop all the cubes", game.SCREEN_WIDTH/2-145, game.SCREEN_HEIGHT/2 + 200);

		graphics2D.setColor(new Color(0x418AFF));
		graphics2D.setFont(new Font("TimesRoman", Font.ITALIC, 30));
		graphics2D.drawString("Press Spacebar", game.SCREEN_WIDTH/2-140, game.SCREEN_HEIGHT/2 + 300);
	}

	@Override
	public Dimension getPreferredSize() {
		return MainGameEngine.FRAME_SIZE;
	}

	public synchronized void updateGame(MainGameEngine game) {
		this.game=game;
	}
}