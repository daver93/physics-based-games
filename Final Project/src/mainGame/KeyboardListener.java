package mainGame;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class KeyboardListener extends KeyAdapter {
	// Author David Triantafyllou

	public static boolean upKeyPressed;
	public static boolean downKeyPressed;
	public static boolean spacebarKeyPressed;

	public static boolean isUpKeyPressed() {
		return upKeyPressed;
	}

	public static boolean isDownKeyPressed() {
		return downKeyPressed;
	}

	public static boolean isSpaceKeyPressed() {
		return spacebarKeyPressed;
	}

	public void keyPressed(KeyEvent e) {
		int key = e.getKeyCode();

		switch (key) {
			case KeyEvent.VK_UP:
				upKeyPressed =true;
				break;
			case KeyEvent.VK_DOWN:
				downKeyPressed = true;
				break;
			case KeyEvent.VK_SPACE:
				spacebarKeyPressed = true;
				break;
		}
	}

	public void keyReleased(KeyEvent e) {
		int key = e.getKeyCode();

		switch (key) {
			case KeyEvent.VK_UP:
				upKeyPressed =false;
				break;
			case KeyEvent.VK_DOWN:
				downKeyPressed = false;
				break;
			case KeyEvent.VK_SPACE:
				spacebarKeyPressed = false;
				MainGameEngine.shouldFire = true;
				break;
		}
	}
}
