package mainGame;

import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.joints.*;
import org.jbox2d.common.Vec2;

/*
 * Author David Triantafyllou
 * Class in which all the joints for the game are implemented
 */

public class CreateJoints {

    public CreateJoints(){

    }

    public void connectShootingWithBase(BasicRectangle base, ShootingCannon theCannon){

        RevoluteJointDef revoluteJointDef = new RevoluteJointDef();

        //revoluteJointDef.initialize(base.body, theCannon.body, theCannon.body.getPosition());

        revoluteJointDef.bodyA = base.body;
        revoluteJointDef.bodyB = theCannon.body;

        revoluteJointDef.collideConnected = false;
        revoluteJointDef.enableMotor = true;
        revoluteJointDef.maxMotorTorque = 100f;

        revoluteJointDef.localAnchorA = new Vec2(0f, 0f);
        revoluteJointDef.localAnchorB = new Vec2(theCannon.body.getPosition().x - theCannon.width, -theCannon.body.getLocalCenter().y/2f);

        MainGameEngine.world.createJoint(revoluteJointDef);
    }

    public void connectPendantWithBase(BasicRectangle base, LevelObjects levelObjects){

        RevoluteJointDef revoluteJointDef = new RevoluteJointDef();
        revoluteJointDef.bodyA = base.body;
        revoluteJointDef.bodyB = levelObjects.body;

        revoluteJointDef.collideConnected = false;
        revoluteJointDef.enableMotor = false;

        revoluteJointDef.localAnchorA = new Vec2(0f, 0);
        revoluteJointDef.localAnchorB = new Vec2(0, levelObjects.height/2);

        revoluteJointDef.enableLimit = true;
        revoluteJointDef.lowerAngle = -85 * org.jbox2d.common.MathUtils.DEG2RAD;
        revoluteJointDef.upperAngle = 85 * org.jbox2d.common.MathUtils.DEG2RAD;

        MainGameEngine.world.createJoint(revoluteJointDef);
    }

    public void connectPendantWithPendantInTheDownMiddle(LevelObjects levelObjects1, LevelObjects levelObjects2){
        //pendant1 should be the vertical one (the bottom one)
        //pendant2 should be the horizontal one (the top one)

        RevoluteJointDef revoluteJointDef = new RevoluteJointDef();
        revoluteJointDef.bodyA = levelObjects1.body;
        revoluteJointDef.bodyB = levelObjects2.body;

        revoluteJointDef.collideConnected = false;
        revoluteJointDef.enableMotor = false;

        revoluteJointDef.localAnchorA = new Vec2(0, levelObjects2.height/2);
        revoluteJointDef.localAnchorB = new Vec2(0f, 0);

        revoluteJointDef.enableLimit = true;
        revoluteJointDef.lowerAngle = -45 * org.jbox2d.common.MathUtils.DEG2RAD;
        revoluteJointDef.upperAngle = 45 * org.jbox2d.common.MathUtils.DEG2RAD;

        MainGameEngine.world.createJoint(revoluteJointDef);
    }

    public void connectTargetWithPendantInTheDownMiddle(BasicPolygon target, LevelObjects levelObjects){
        //pendant1 should be the vertical one (the bottom one)
        //pendant2 should be the horizontal one (the top one)

        RevoluteJointDef revoluteJointDef = new RevoluteJointDef();
        revoluteJointDef.bodyA = target.body;
        revoluteJointDef.bodyB = levelObjects.body;

        revoluteJointDef.collideConnected = false;
        revoluteJointDef.enableMotor = false;

        revoluteJointDef.localAnchorA = new Vec2(0, target.getPolygonRadius());
        revoluteJointDef.localAnchorB = new Vec2(0f, -levelObjects.height/2);

        revoluteJointDef.enableLimit = true;
        revoluteJointDef.lowerAngle = -45 * org.jbox2d.common.MathUtils.DEG2RAD;
        revoluteJointDef.upperAngle = 45 * org.jbox2d.common.MathUtils.DEG2RAD;

        MainGameEngine.world.createJoint(revoluteJointDef);
    }

    public void createPulleyJoint(Body bodyA, Body bodyB, Body groundBody){

        PulleyJointDef pulleyJointDef = new PulleyJointDef();
        pulleyJointDef.bodyA = bodyA;
        pulleyJointDef.bodyB = bodyB;

        pulleyJointDef.collideConnected = false;
        pulleyJointDef.localAnchorA.set(0, 0);
        pulleyJointDef.localAnchorB.set(0, 0);

        //Maximum length of each of the ropes (for each body) [in meters]
        pulleyJointDef.lengthA = 5f;
        pulleyJointDef.lengthB = 5f;

        //ground anchors --> where I want to tide each of the bodies
        pulleyJointDef.groundAnchorA.set(bodyA.getPosition().x, groundBody.getPosition().y);
        pulleyJointDef.groundAnchorB.set(bodyB.getPosition().x, groundBody.getPosition().y);

        //the default value of the ratio is 1. The value 1 means equal balance
        pulleyJointDef.ratio = 1.0f;

        MainGameEngine.world.createJoint(pulleyJointDef);
    }
}
