package mainGame;

import java.awt.Color;
import java.awt.Dimension;
import java.util.*;

import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.BodyType;
import org.jbox2d.dynamics.World;
import org.jbox2d.dynamics.Fixture;


public class MainGameEngine {
	
	// frame dimensions
    public static final float factor = 2.0f;

	public static final int SCREEN_HEIGHT = 720;
	public static final int SCREEN_WIDTH = 960;
	public static final Dimension FRAME_SIZE = new Dimension(SCREEN_WIDTH, SCREEN_HEIGHT);
	public static final float WORLD_WIDTH = 10 * factor;//metres
	public static final float WORLD_HEIGHT=SCREEN_HEIGHT*(WORLD_WIDTH/SCREEN_WIDTH);// meters - keeps world dimensions in same aspect ratio as screen dimensions, so that circles get transformed into circles as opposed to ovals
	public static final float GRAVITY=9.8f;

	public static World world; // Box2D container for all bodies and barriers 

	// sleep time between two drawn frames in milliseconds 
	public static final int DELAY = 20;
	public static final int NUM_EULER_UPDATES_PER_SCREEN_REFRESH=100;
	// estimate for time between two frames in seconds 
	public static final float DELTA_T = DELAY / 1000.0f;

	public static int convertWorldXtoScreenX(float worldX) {
		return (int) (worldX/WORLD_WIDTH*SCREEN_WIDTH);
	}
	public static int convertWorldYtoScreenY(float worldY) {
		// minus sign in here is because screen coordinates are upside down.
		return (int) (SCREEN_HEIGHT-(worldY/WORLD_HEIGHT*SCREEN_HEIGHT));
	}
	public static float convertWorldLengthToScreenLength(float worldLength) {
		return (worldLength/WORLD_WIDTH*SCREEN_WIDTH);
	}

	public List<BasicParticle> particles;
	public List<BasicPolygon> targets;

	public ArrayList<LevelObjects> borders;
    public ArrayList<LevelObjects> walls;
    public ArrayList<LevelObjects> platforms;
	public ArrayList<LevelObjects> levelObjects;
	public ArrayList<BasicRectangle> floors = new ArrayList<BasicRectangle>();

	public boolean isWinner;
	public int numberOfTries;

    public static KeyboardListener keyListener;

	float rollingFriction;

	public BasicRectangle canonBase01;

	public ShootingCannon shootingCannon;

	public int level = 1; // This variable stores the current level for the player
	public final int maximumLevelNumber = 7; // This variable is final, and it has the maximum number of level (actually, is the number of the last level)
	public static CreateJoints createJoints;

	// Constructor for the class which are doing the main implementation of the game
	public MainGameEngine() {
	    // Author David Triantafyllou

		world = new World(new Vec2(0, -GRAVITY));// create Box2D container for everything
		world.setContinuousPhysics(true);

		// Create all the lists. Some lists are of the same type, but they exist to handle them in different way
		particles = new ArrayList<BasicParticle>();
		targets = new ArrayList<BasicPolygon>();
		walls = new ArrayList<LevelObjects>();
		borders = new ArrayList<LevelObjects>();
		levelObjects = new ArrayList<LevelObjects>();
		platforms = new ArrayList<LevelObjects>();

		// isWinner is used to check if the game ended or not
		isWinner = false;

		rollingFriction=.02f;

		//create the borders for the game
        createBorders();

        //initialize the number of tries and start loading the level
		numberOfTries = 0;
		loadLevel(level);
	}

	public static void main(String[] args) throws Exception {
	    // Author David Triantafyllou

		final MainGameEngine game = new MainGameEngine();
		final BasicView view = new BasicView(game);
		JEasyFrame frame = new JEasyFrame(view, "David Triantafyllou - Physics Game");

		// Add listener for keyboard
        keyListener = new KeyboardListener();
		frame.addKeyListener(keyListener);

		// Added listener for collisions
		world.setContactListener(new CollisionHandler());

		// Start game in a new thread
		game.startThread(view);
	}

	public float time;
	private void startThread(final BasicView view) throws InterruptedException {
		final MainGameEngine game=this;
		while (true) {

			long start = System.currentTimeMillis();

			if (view.showSplashScreen){
				if (keyListener.isSpaceKeyPressed()){
					view.showSplashScreen = false;
				}
			}
			else {
				game.update();
			}

			view.repaint();

			try {
				Thread.sleep(DELAY);
			} catch (InterruptedException e) {
			}

			long end = System.currentTimeMillis();

			if (!view.showSplashScreen){
				// Calculate the elapsed time
				time += (end - start) / 1000F;
			}
		}
	}

	public void update() {

        changeCannonRotation();
		moveCannon();

        // Here is the collision check in order to see if the joint must be destroyed or not
        if (CollisionHandler.ballHitPendant()){
        	//The destroy of the joint must be occurred here, because during the callback this action is not possible (locked)
			world.destroyJoint(CollisionHandler.getJoint());
		}

		int VELOCITY_ITERATIONS=NUM_EULER_UPDATES_PER_SCREEN_REFRESH;
		int POSITION_ITERATIONS=NUM_EULER_UPDATES_PER_SCREEN_REFRESH;
		for (BasicParticle p:particles) {
			// give the objects an opportunity to add any bespoke forces, e.g. rolling friction
			p.notificationOfNewTimestep();

		}
		for (BasicPolygon p: targets) {
			// give the objects an opportunity to add any bespoke forces, e.g. rolling friction
			p.notificationOfNewTimestep();
		}

		for (BasicRectangle floor : floors){
			floor.notificationOfNewTimestep();
		}

		checkIfAllProjectilesAreDown();
		destroyBalls();

		if (CollisionHandler.shouldDestroyObject()){
			destroyTarget(CollisionHandler.getObjectToDestroy());
		}

		world.step(DELTA_T, VELOCITY_ITERATIONS, POSITION_ITERATIONS);
	}

	public void createBorders(){
		//Author David Triantafyllou

		//Create the borders of the board
		borders.add(new LevelObjects(WORLD_WIDTH/2, 0, 0, 0, new Color(0x6D6C82), 0, 0.1f, WORLD_WIDTH, 0.1f, BodyType.STATIC, "border"));
		borders.add(new LevelObjects(WORLD_WIDTH/2, WORLD_HEIGHT, 0, 0, new Color(0x6D6C82), 0, 0.1f, WORLD_WIDTH, 0.1f, BodyType.STATIC, "border"));

		borders.add(new LevelObjects(0, WORLD_HEIGHT/2, 0, 0, new Color(0x6D6C82), 0, 0.1f, 0.1f, WORLD_HEIGHT, BodyType.STATIC, "border"));
		borders.add(new LevelObjects(WORLD_WIDTH, WORLD_HEIGHT/2, 0, 0, new Color(0x6D6C82), 0, 0.1f, 0.1f, WORLD_HEIGHT, BodyType.STATIC, "border"));
	}

	public float angle = 0;
	public static boolean shouldFire;

	public void moveCannon(){
		// When the cannon reaches the specified position (if it is moving), it changes its linear velocity to the opposite
		if (canonBase01.body.getLinearVelocity() != new Vec2(0, 0)){
			if (canonBase01.body.getPosition().y >= 11f || canonBase01.body.getPosition().y <= 2){
				canonBase01.body.setLinearVelocity(new Vec2(0, -canonBase01.body.getLinearVelocity().y));
			}
		}
	}

	public void changeCannonRotation(){
		//Author David Triantafyllou

	    if (keyListener.isUpKeyPressed() && shootingCannon.body.getAngle() < 1f){
			shootingCannon.body.setAngularVelocity(.5f);
        }
	    else if (keyListener.isDownKeyPressed() && shootingCannon.body.getAngle() > -1f){
			shootingCannon.body.setAngularVelocity(-.5f);
		}
	    else if (keyListener.isSpaceKeyPressed()){
			fire();
			keyListener.spacebarKeyPressed = false;
		}
	    else {
			shootingCannon.body.setAngularVelocity(.0f);
		}
    }

	public void fire(){
		//Author David Triantafyllou

		if (shouldFire){
		    numberOfTries++;
			initialVelocity = 15; // V
			float velocityX = initialVelocity * org.jbox2d.common.MathUtils.cos(shootingCannon.body.getAngle()); // Vx
			float velocityY = initialVelocity * org.jbox2d.common.MathUtils.sin(shootingCannon.body.getAngle()); // Vy

            // Create the fire ball with the given velocity (starting point is the edge of the cannon
			particles.add(new BasicParticle(shootingCannon.body.getLocalCenter().x + shootingCannon.body.getWorldPoint(shootingCannon.vertices[2]).x,shootingCannon.body.getLocalCenter().y + shootingCannon.body.getWorldPoint(shootingCannon.vertices[2]).y,velocityX,velocityY, 0.2f, Color.RED, ballMass, 0, "fireball"));
			shouldFire = false;
		}
	}

	public float initialVelocity;

	public void destroyBalls() throws ConcurrentModificationException {
		//Author David Triantafyllou

		try {
            //Destroy the ball if it reached the bottom border of the world
			for (BasicParticle particle : particles) {
				float posY = particle.body.getWorldCenter().y - particle.getParticleRadius();
				if (posY <= 0.3) {
					particles.remove(particle.body);

					Fixture list = particle.body.getFixtureList();
					if (list != null) {
						particle.body.destroyFixture(list);
					}
				}
				else {
				    //Destroy the ball if it reached the left or the right border of the world
				    if (particle.body.getWorldCenter().x - particle.getParticleRadius() <= 0.3f  || particle.body.getWorldCenter().x + particle.getParticleRadius() >= WORLD_WIDTH-0.3f ){
                        particles.remove(particle.body);

                        Fixture list = particle.body.getFixtureList();
                        if (list != null) {
                            particle.body.destroyFixture(list);
                        }
                    }
                }
			}
		} catch (ConcurrentModificationException e) {
            // DO NOTHING
		}
	}

	public void checkIfAllProjectilesAreDown() throws ConcurrentModificationException{
		//Author David Triantafyllou

    	if (targets.isEmpty()){
    		if (level >= maximumLevelNumber){
				isWinner = true;
                finalDestroyAllObjects();
			}
			else {
				++level;
				numberOfTries = 0;
				loadLevel(level);
			}
		}
    	else {
    	    destroyPolygons();
		}
	}

	// The following method destroyPolygons() is being used to fix a bug that occurs some times
	public void destroyPolygons(){
		try {
			for (BasicPolygon projectile : targets){
				if (projectile.body.getWorldCenter().y - projectile.getPolygonRadius() <= 0){
					targets.remove(projectile);

					Fixture list = projectile.body.getFixtureList();
					if (list != null){
						projectile.body.destroyFixture(list);
					}
				}
			}
		} catch (ConcurrentModificationException e){
			// DO NOTHING
		}
	}

	public void destroyTarget(Body bodyTarget){
		// Author David Triantafyllou

		//This method is being called when an object must be destroyed (from the targets)

		try {
			Fixture list = bodyTarget.getFixtureList();
			if (list != null){
				bodyTarget.destroyFixture(list);
			}

			// In the following loop, the code identifies which of the targets must be removed from the list
			for (BasicPolygon projectile : targets){
				if (projectile.body.equals(bodyTarget)){
					targets.remove(projectile);
					CollisionHandler.changeToFalse();
					break;
				}
			}

		} catch (ConcurrentModificationException e){
			// DO NOTHING
		}
	}

	public void loadLevel(float level){
		//Author David Triantafyllou

	    float cannonWidth = 0.4f;
	    float cannonHeight = 4f;

		canonBase01 = new BasicRectangle(0.2f, WORLD_HEIGHT, 0, 0, Color.GREEN, 1, 1, cannonWidth, cannonHeight);
		shootingCannon = new ShootingCannon(canonBase01.body.getPosition().x + (cannonWidth * 2) * 2, canonBase01.body.getPosition().y, 0, 0, Color.GREEN, 5, 0, 2f, 0.2f);

		createJoints = new CreateJoints();
		createJoints.connectShootingWithBase(canonBase01, shootingCannon);

		if (level == 1){
			System.out.println("Level 1 is loading...");

			destroyLevelObjects();

			canonBase01.body.setLinearVelocity(new Vec2(0, 0));
			floors.add(new BasicRectangle(WORLD_WIDTH, WORLD_HEIGHT, 0, 0, Color.ORANGE, 0, 1, 5f, 0.1f));
			createObjectsToHit(4, floors.get(0).body);
		}
		else if (level == 2) {
			System.out.println("Level 2 is loading...");

			destroyLevelObjects();

			canonBase01.body.setLinearVelocity(new Vec2(0, 0));

			floors.add(new BasicRectangle(WORLD_WIDTH + 2, WORLD_HEIGHT - 2, 0, 0, new Color(0x450300), 0, 1, 4f, 0.1f));
			floors.add(new BasicRectangle(WORLD_WIDTH + 6, WORLD_HEIGHT - 12, 0, 0, new Color(0x450300), 0, 1.5f, 4f, 0.1f));

			LevelObjects levelObjects = new LevelObjects(floors.get(0).body.getWorldCenter().x, (floors.get(0).body.getWorldCenter().y - 1) * 2 , 0, 0, Color.BLUE, 6, 1, 0.1f, 4);
			createJoints.connectPendantWithBase(floors.get(0), levelObjects);
			this.levelObjects.add(levelObjects);

			createObjectsToHit(3, floors.get(0).body);
			createObjectsToHit(4, floors.get(1).body);
		}
		else if (level == 3){
			System.out.println("Level 3 is loading...");

			destroyLevelObjects();

			// In this the level, the cannon is moving to make it more difficult
			canonBase01.body.setLinearVelocity(new Vec2(0, 1.5f));

            floors.add(new BasicRectangle(WORLD_WIDTH + 2, WORLD_HEIGHT - 2, 0, 0, new Color(0x094522), 0, 1, 4f, 0.1f));
            floors.add(new BasicRectangle(WORLD_WIDTH + 6, WORLD_HEIGHT - 12, 0, 0, new Color(0x094522), 0, 1.5f, 4f, 0.1f));

            LevelObjects levelObjects = new LevelObjects(floors.get(0).body.getWorldCenter().x, floors.get(0).body.getWorldCenter().y - 1.9f, 0, 0, Color.BLUE, 6, 1, 0.1f, 3.8f, BodyType.STATIC);
            createJoints.connectPendantWithBase(floors.get(0), levelObjects);
            this.levelObjects.add(levelObjects);

            createObjectsToHit(3, floors.get(0).body);
            createObjectsToHit(4, floors.get(1).body);
		}
		else if (level == 4){
			System.out.println("Level 4 is loading...");

			destroyLevelObjects();

			float firstPendantHeight = 6.0f;
			float firstPendantWidth = 0.3f;
			float offset = 1;

			levelObjects.add(new LevelObjects(WORLD_WIDTH/2-6, 0+firstPendantHeight/2-offset, 0, 0, new Color(0x1D8D47), 10, 1, firstPendantWidth, firstPendantHeight, BodyType.STATIC));
            levelObjects.add(new LevelObjects(WORLD_WIDTH/2-6, WORLD_HEIGHT-firstPendantHeight/2-offset, 0, 0, new Color(0x8D0303), 10, 1, firstPendantWidth, WORLD_HEIGHT-firstPendantHeight, BodyType.STATIC));

            //the y position of each pendant should be the half of the height value (to be on the bottom) + how much higher from the bottom we want it to be
			levelObjects.add(new LevelObjects(WORLD_WIDTH/2, 0.1f+0.5f, 0, 0, new Color(0x2F8D27), 3, 1, 4f, 0.2f));
			levelObjects.add(new LevelObjects(WORLD_WIDTH/2, 0.5f, 0, 0, new Color(0x04008D), 5, 1, 0.2f, 1.0f, BodyType.STATIC));

			createJoints.connectPendantWithPendantInTheDownMiddle(levelObjects.get(3), levelObjects.get(2));

			levelObjects.add(new LevelObjects(WORLD_WIDTH/2+3, 2f, 0, 0, new Color(0x2F8D27), 2, 5f, 3f, 0.2f));
			levelObjects.add(new LevelObjects(WORLD_WIDTH/2+3, 1f, 0, 0, new Color(0x04008D), 5, 1, 0.2f, 2f, BodyType.STATIC));

			createJoints.connectPendantWithPendantInTheDownMiddle(levelObjects.get(5), levelObjects.get(4));

			targets.add(new BasicPolygon(levelObjects.get(4).body.getWorldCenter().x-1.2f,2f+0.1f,0,0, 0.3f,new Color(0x008D0C), 0.1f, 0.01f,4, "polygon"));
		}
		else if (level == 5){
			System.out.println("Level 5 is loading...");

		    destroyLevelObjects();

			// In this the level, the cannon is moving to make it more difficult
			canonBase01.body.setLinearVelocity(new Vec2(0, 1.5f));

            float firstPendantHeight = 6.0f;
            float firstPendantWidth = 0.3f;
            float offset = 1;

            levelObjects.add(new LevelObjects(WORLD_WIDTH/2-6, 0+firstPendantHeight/2-offset, 0, 0, new Color(0x1D8D47), 10, 1, firstPendantWidth, firstPendantHeight, BodyType.STATIC));
            levelObjects.add(new LevelObjects(WORLD_WIDTH/2-6, WORLD_HEIGHT-firstPendantHeight/2-offset, 0, 0, new Color(0x8D0303), 10, 1, firstPendantWidth, WORLD_HEIGHT-firstPendantHeight, BodyType.STATIC));

            //the y position of each pendant should be the half of the height value (to be on the bottom) + how much higher from the bottom we want it to be
            levelObjects.add(new LevelObjects(WORLD_WIDTH/2, 0.1f+0.5f, 0, 0, new Color(0x2F8D27), 3, 1, 4f, 0.2f));
            levelObjects.add(new LevelObjects(WORLD_WIDTH/2, 0.5f, 0, 0, new Color(0x04008D), 3, 1, 0.2f, 1.0f, BodyType.STATIC));

            createJoints.connectPendantWithPendantInTheDownMiddle(levelObjects.get(3), levelObjects.get(2));

            levelObjects.add(new LevelObjects(WORLD_WIDTH/2+3, 2f, 0, 0, new Color(0x2F8D27), 2, 1, 3f, 0.2f));
            levelObjects.add(new LevelObjects(WORLD_WIDTH/2+3, 1f, 0, 0, new Color(0x04008D), 5, 1, 0.2f, 2f, BodyType.STATIC));

            createJoints.connectPendantWithPendantInTheDownMiddle(levelObjects.get(5), levelObjects.get(4));

            targets.add(new BasicPolygon(levelObjects.get(4).body.getWorldCenter().x-1.2f,2f+0.1f,0,0, 0.3f,new Color(0x008D0C), 0.1f, 0.01f,4, "polygon"));

            levelObjects.add(new LevelObjects(WORLD_WIDTH/2+5, 3f, 0, 0, new Color(0x2F8D27), 8, 1, 3f, 0.2f));
            levelObjects.add(new LevelObjects(WORLD_WIDTH/2+5, 1.5f, 0, 0, new Color(0x04008D), 5, 1, 0.2f, 3f, BodyType.STATIC));

            createJoints.connectPendantWithPendantInTheDownMiddle(levelObjects.get(7), levelObjects.get(6));

			targets.add(new BasicPolygon(levelObjects.get(4).body.getWorldCenter().x-1.2f,2f+0.1f,0,0, 0.3f,new Color(0x008D0C), 0.1f, 0.01f,4, "polygon"));
			createObjectsToHit(2, levelObjects.get(6).body);
        }
		else if (level == 6){
			System.out.println("Level 6 is loading...");

			destroyLevelObjects();

			float wallWidth = 5.0f;
			float wallHeight = 0.2f;

			float wallPosX = WORLD_WIDTH / 2;
			float wassPosY = WORLD_HEIGHT-wallHeight/2;

			walls.add(new LevelObjects(wallPosX, wassPosY, 0, 0, new Color(0x77410E), 0, 0, wallWidth, wallHeight, BodyType.STATIC));
			walls.add(new LevelObjects(wallPosX + wallWidth, wassPosY, 0, 0, new Color(0x77410E), 0, 0, wallWidth, wallHeight, BodyType.STATIC));

			float firstPendantHeight = 3.0f;
			float firstPendantWidth = 0.15f;

			levelObjects.add(new LevelObjects(wallPosX + wallWidth, wassPosY - firstPendantHeight/2, 0, 0, new Color(0x1D8D47), 3f, 1, firstPendantWidth, firstPendantHeight, BodyType.DYNAMIC, "pendantΗοοκ"));

			createJoints.connectPendantWithPendantInTheDownMiddle(levelObjects.get(0), walls.get(1));

			float targetRadius = 0.4f;

			targets.add(new BasicPolygon(levelObjects.get(0).body.getWorldCenter().x, levelObjects.get(0).body.getWorldCenter().y-firstPendantHeight/2,0,0, targetRadius,new Color(0x008D0C), 1f, 0.0f,4, "polygon"));

			createJoints.connectTargetWithPendantInTheDownMiddle(targets.get(0), levelObjects.get(0));

			walls.add(new LevelObjects(WORLD_WIDTH/2, 6.5f, 0, 0, new Color(0x77410E), 0f, 0, wallWidth, WORLD_HEIGHT-2, BodyType.STATIC));
		}
		else if (level == 7){
			System.out.println("Level 7 is loading...");

			destroyLevelObjects();

			walls.clear();
			walls.add(new LevelObjects(WORLD_WIDTH/2 - 1, WORLD_HEIGHT/2 + 4.55f, 0, 0, new Color(0x77410E), 0, 0, 10, 0.1f, BodyType.STATIC));
			walls.add(new LevelObjects(WORLD_WIDTH/2 - 1, WORLD_HEIGHT/2-1.5f, 0, 0, new Color(0x772A1F), 0, 0, 0.2f, 12f, BodyType.STATIC));

			platforms.add(new LevelObjects(WORLD_WIDTH/2 - 5,WORLD_HEIGHT/2, 0, 0, new Color(0x004016), 5f, 0.1f, 3f, 0.2f, BodyType.DYNAMIC));
            platforms.add(new LevelObjects(WORLD_WIDTH/2 + 3,WORLD_HEIGHT/2, 0, 0, new Color(0x004016), 5f, 0.1f, 3f, 0.2f, BodyType.DYNAMIC));

			createJoints.createPulleyJoint(platforms.get(0).body, platforms.get(1).body, walls.get(0).body);

			targets.add(new BasicPolygon(platforms.get(1).body.getWorldCenter().x + platforms.get(1).width/2-0.5f,platforms.get(1).body.getWorldCenter().y,0,0, 0.5f, new Color(0x008D0C), 0.1f, 0.0f,4, BodyType.DYNAMIC, "polygon"));
			targets.add(new BasicPolygon(platforms.get(1).body.getWorldCenter().x - platforms.get(1).width/2+0.5f,platforms.get(1).body.getWorldCenter().y,0,0, 0.5f, new Color(0x008D0C), 0.1f, 0.0f,4, BodyType.DYNAMIC, "polygon"));
		}
		else {
			// DO NOTHING: It's the end of the game
        }
	}

    float ballMass = 10f;

	public void finalDestroyAllObjects(){
		//Author David Triantafyllou

        if (canonBase01.body.getFixtureList() != null){
            canonBase01.body.destroyFixture(canonBase01.body.getFixtureList());
        }
        if (shootingCannon.body.getFixtureList() != null){
            shootingCannon.body.destroyFixture(shootingCannon.body.getFixtureList());
        }
    }

    //destroyLevelObjects() : destroy the objects that each level may have. This method should be called before set up the next level
    public void destroyLevelObjects(){
		//Author David Triantafyllou
		try {
			for (LevelObjects p : levelObjects){
				Fixture list = p.body.getFixtureList();
				if (list != null){
					p.body.destroyFixture(list);
					if (p.body.getJointList() != null){
						world.destroyJoint(p.body.getJointList().joint);
					}
				}
			}
			for (BasicRectangle floor : floors){
				if (floor.body.getFixtureList() != null){
					floor.body.destroyFixture(floor.body.getFixtureList());
					if (floor.body.getJointList() != null){
						world.destroyJoint(floor.body.getJointList().joint);
					}
				}
			}
			for (LevelObjects wall : walls){
				if (wall.body.getFixtureList() != null){
					wall.body.destroyFixture(wall.body.getFixtureList());
					if (wall.body.getJointList() != null){
						world.destroyJoint(wall.body.getJointList().joint);
					}
				}
			}
			for (LevelObjects platform : platforms){
				if (platform.body.getFixtureList() != null){
					platform.body.destroyFixture(platform.body.getFixtureList());
					if (platform.body.getJointList() != null){
						world.destroyJoint(platform.body.getJointList().joint);
					}
				}
			}
			for (BasicParticle ball : particles){
				if (ball.body.getFixtureList() != null){
					ball.body.destroyFixture(ball.body.getFixtureList());
					if (ball.body.getJointList() != null){
						world.destroyJoint(ball.body.getJointList().joint);
					}
				}
			}

		} catch (ConcurrentModificationException cme){
			// DO NOTHING
		}

		// clear the lists
		levelObjects.clear();
		floors.clear();
		walls.clear();
		platforms.clear();
		targets.clear();
	}

    public void createObjectsToHit(int counter, Body body){
		//Author David Triantafyllou

		float incrX = body.getWorldCenter().x;
		float incrY = body.getWorldCenter().y + 0.1f;
		float posX = incrX + 0.1f;

		//Generate the projectiles that the player will try to throw down
		for (float i = incrY; i < 12; i++){
			for (int j = 0; j < counter; j++){
				targets.add(new BasicPolygon(posX,i,0,0, 0.3f*1.2f, pickNextColor(), 1, 0.02f,4, "polygon"));
				posX -= 0.1f;
			}
			pickNextColor++;
			counter--;
		}
	}

	private int pickNextColor = 0;
	public Color pickNextColor(){
		ArrayList<Color> colors = new ArrayList<>();
		colors.add(new Color(0x1E914E));
		colors.add(new Color(0x398591));
		colors.add(new Color(0x917F25));
		colors.add(new Color(0x911E33));

		if (pickNextColor >= colors.size()){
			pickNextColor = 0;
		}

		return colors.get(pickNextColor);
	}
}


