package mainGame;

import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.dynamics.*;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Path2D;

public class LevelObjects {

    /*
     * Author David Triantafyllou
     */

    public final float ratioOfScreenScaleToWorldScale;

    protected final float rollingFriction,mass;
    public Color col;

    public static float width;
    public static float height;

    protected org.jbox2d.common.Vec2[] vertices;

    public Body body;
    public World w;
    public BodyDef bodyDef;
    public PolygonShape shape;
    public FixtureDef fixtureDef;

    public LevelObjects(float sx, float sy, float vx, float vy, Color col, float mass, float rollingFriction, float width, float height) {
        this(sx, sy, vx, vy, col, mass, rollingFriction, width, height, 0, BodyType.DYNAMIC, null, null);
    }

    public LevelObjects(float sx, float sy, float vx, float vy, Color col, float mass, float rollingFriction, float width, float height, float angle) {
        this(sx, sy, vx, vy, col, mass, rollingFriction, width, height, angle, BodyType.DYNAMIC, null, null);
    }

    public LevelObjects(float sx, float sy, float vx, float vy, Color col, float mass, float rollingFriction, float width, float height, BodyType bodyType) {
        this(sx, sy, vx, vy, col, mass, rollingFriction, width, height, 0, bodyType, null, null);
    }

    public LevelObjects(float sx, float sy, float vx, float vy, Color col, float mass, float rollingFriction, float width, float height, Object userData) {
        this(sx, sy, vx, vy, col, mass, rollingFriction, width, height, 0, BodyType.DYNAMIC, userData, null);
    }

    public LevelObjects(float sx, float sy, float vx, float vy, Color col, float mass, float rollingFriction, float width, float height, float angle, Object userData) {
        this(sx, sy, vx, vy, col, mass, rollingFriction, width, height, angle, BodyType.DYNAMIC, userData, null);
    }

    public LevelObjects(float sx, float sy, float vx, float vy, Color col, float mass, float rollingFriction, float width, float height, BodyType bodyType, Object userData) {
        this(sx, sy, vx, vy, col, mass, rollingFriction, width, height, 0, bodyType, userData, null);
    }

    public LevelObjects(float sx, float sy, float vx, float vy, Color col, float mass, float rollingFriction, float width, float height, Color color) {
        this(sx, sy, vx, vy, col, mass, rollingFriction, width, height, 0, BodyType.DYNAMIC, null, color);
    }

    public LevelObjects(float sx, float sy, float vx, float vy, Color col, float mass, float rollingFriction, float width, float height, float angle, Color color) {
        this(sx, sy, vx, vy, col, mass, rollingFriction, width, height, angle, BodyType.DYNAMIC, null, color);
    }

    public LevelObjects(float sx, float sy, float vx, float vy, Color col, float mass, float rollingFriction, float width, float height, BodyType bodyType, Color color) {
        this(sx, sy, vx, vy, col, mass, rollingFriction, width, height, 0, bodyType, null, color);
    }

    public LevelObjects(float sx, float sy, float vx, float vy, Color col, float mass, float rollingFriction, float width, float height, Object userData, Color color) {
        this(sx, sy, vx, vy, col, mass, rollingFriction, width, height, 0, BodyType.DYNAMIC, userData, color);
    }

    public LevelObjects(float sx, float sy, float vx, float vy, Color col, float mass, float rollingFriction, float width, float height, float angle, Object userData, Color color) {
        this(sx, sy, vx, vy, col, mass, rollingFriction, width, height, angle, BodyType.DYNAMIC, userData, color);
    }

    public LevelObjects(float sx, float sy, float vx, float vy, Color col, float mass, float rollingFriction, float width, float height, BodyType bodyType, Object userData, Color color) {
        this(sx, sy, vx, vy, col, mass, rollingFriction, width, height, 0, bodyType, userData, color);
    }


    public LevelObjects(float sx, float sy, float vx, float vy, Color col, float mass, float rollingFriction, float width, float height, float angle, BodyType bodyType, Object userData, Color color){

        this.width = width;
        this.height = height;

        this.ratioOfScreenScaleToWorldScale = MainGameEngine.convertWorldLengthToScreenLength(1);
        this.col = col;
        this.rollingFriction = rollingFriction;
        this.mass = mass;

        polygonPath = mkRegularRectangle(width, height);

        w = MainGameEngine.world;

        bodyDef = new org.jbox2d.dynamics.BodyDef();

        if (angle != 0){
            bodyDef.setAngle(angle);
        }
        bodyDef.type = bodyType;
        bodyDef.position.set(sx, sy);

        bodyDef.angularDamping = 1f;
        if (userData == null){
            bodyDef.setUserData("Pendant");
        }
        else {
            bodyDef.setUserData(userData);
        }

        this.body = w.createBody(bodyDef);

        shape = new org.jbox2d.collision.shapes.PolygonShape();
        vertices = verticesOfPath2D(polygonPath, 4);
        shape.set(vertices, 4);

        fixtureDef = new org.jbox2d.dynamics.FixtureDef();// This class is from Box2D
        fixtureDef.shape = shape;
        fixtureDef.density = 2 * width * height * mass;
        fixtureDef.friction = 5f;// this is surface friction;
        fixtureDef.restitution = 0.1f;

        body.createFixture(fixtureDef);
    }

    // Vec2 vertices of Path2D
    public static org.jbox2d.common.Vec2[] verticesOfPath2D(Path2D.Float p, int n) {

        org.jbox2d.common.Vec2[] vertices = new org.jbox2d.common.Vec2[4];

        vertices[0]=new org.jbox2d.common.Vec2(-width/2, -height/2);
        vertices[1]=new org.jbox2d.common.Vec2(width/2, -height/2);
        vertices[2]=new org.jbox2d.common.Vec2(width/2, height/2);
        vertices[3]=new org.jbox2d.common.Vec2(-width/2, height/2);

        return vertices;
    }

    protected final Path2D.Float polygonPath;

    //public Color color = Color.GREEN;

    public void draw(Graphics2D g) {
        g.setColor(col);
        org.jbox2d.common.Vec2 position = body.getPosition();
        float angle = body.getAngle();

        AffineTransform af = new AffineTransform();
        af.translate(MainGameEngine.convertWorldXtoScreenX(position.x), MainGameEngine.convertWorldYtoScreenY(position.y));
        af.scale(ratioOfScreenScaleToWorldScale, -ratioOfScreenScaleToWorldScale);// there is a minus in here because screenworld is flipped upsidedown compared to physics world
        af.rotate(angle);

        Path2D.Float p = new Path2D.Float (polygonPath,af);
        g.fill(p);
    }

    public static Path2D.Float mkRegularRectangle(float width, float height) {

        Path2D.Float p = new Path2D.Float();
        float x;
        float y;

        p.moveTo(width/2, height/2);

        x = -width/2;
        y = height/2;
        p.lineTo(x, y);

        x = -width/2;
        y = -height/2;
        p.lineTo(x, y);

        x = width/2;
        y = -height/2;
        p.lineTo(x, y);

        x = width/2;
        y = height/2;
        p.lineTo(x, y);

        p.closePath();
        return p;
    }
}
