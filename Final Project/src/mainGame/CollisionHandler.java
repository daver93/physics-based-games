package mainGame;

import org.jbox2d.callbacks.ContactListener;
import org.jbox2d.collision.Manifold;
import org.jbox2d.dynamics.contacts.Contact;
import org.jbox2d.callbacks.ContactImpulse;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.joints.Joint;

/*
 * Author David Triantafyllou
 * This class is being used to override some functions when Collision with objects is happening
 */

public class CollisionHandler implements ContactListener {
    //NOTE: collisions occur between fixtures (no bodies)

    private static boolean ballHitPendant;
    private static boolean objectTouchBorders;
    private static Joint joint;
    private static Body objectToDestroy;

    @Override
    public void beginContact(Contact contact) {
        Body bodyA = contact.getFixtureA().getBody();
        Body bodyB = contact.getFixtureB().getBody();

        Object bodyUserDataA = bodyA.getUserData();
        Object bodyUserDataB = bodyB.getUserData();

        try{
            if (bodyUserDataA.equals("pendantΗοοκ") && bodyUserDataB.equals("fireball")){
                ballHitPendant = true;
                joint = bodyA.getJointList().joint;
                return;
            }
            else if ( (bodyUserDataA.equals("border") && bodyUserDataB.equals("polygon")) || (bodyUserDataB.equals("border") && bodyUserDataA.equals("polygon")) ){
                objectTouchBorders = true;
                ballHitPendant = false;
                objectToDestroy = bodyB;
            }
            else {
                ballHitPendant = false;
                objectTouchBorders = false;
            }
        } catch (NullPointerException e){
            //DO NOTHING
        }
    }

    @Override
    public void endContact(Contact contact) {
        ballHitPendant = false;
        ballHitPendant = false;
    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {

    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {

    }

    public static boolean ballHitPendant(){
        return ballHitPendant;
    }

    //Return the joint --> We need that in order to destroy it AFTER the collision callback
    public static Joint getJoint(){
        ballHitPendant = false;
        return joint;
    }

    public static boolean shouldDestroyObject(){
        return objectTouchBorders;
    }

    //Return the body of the object to destroy --> We need that in order to destroy it AFTER the collision callback
    public static Body getObjectToDestroy(){
        return objectToDestroy;
    }

    public static void changeToFalse(){
        objectTouchBorders = false;
    }
}
