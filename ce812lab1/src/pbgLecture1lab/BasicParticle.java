package pbgLecture1lab;

import static pbgLecture1lab.BasicPhysicsEngine.DELTA_T;


import java.awt.Color;
import java.awt.Graphics2D;

public class BasicParticle {
	private final int RADIUS_OF_PARTICLE_IN_PIXELS;

	//private double sx,sy,vx,vy;

	public Vect2D pos = new Vect2D();
	public Vect2D vel = new Vect2D();
	
	private final double radius;
	private final Color col;
	private final boolean improvedEuler;
	

	public BasicParticle(double sx, double sy, double vx, double vy, double radius, boolean improvedEuler, Color col) {
		pos.x=sx;
		pos.y=sy;
		vel.x=vx;
		vel.y=vy;
		this.radius=radius;
		this.improvedEuler=improvedEuler;
		this.RADIUS_OF_PARTICLE_IN_PIXELS=Math.max(BasicPhysicsEngine.convertWorldLengthToScreenLength(radius),1);
		this.col=col;
	}

	public void update() {
		Vect2D acc = new Vect2D(0, -BasicPhysicsEngine.GRAVITY);
		Vect2D trialAcc = new Vect2D(0, -BasicPhysicsEngine.GRAVITY);

		if (improvedEuler) {
			// improved Euler
			//TODO

			//trialVel = vel.add(trialVel.addScaled(acc, DELTA_T)) ;
			Vect2D trialVel = vel.addScaled(acc, DELTA_T) ;

			pos = pos.addScaled(vel.add(trialVel),0.5*DELTA_T);

			vel = vel.addScaled(acc.add(trialAcc),0.5*DELTA_T);

		} else {
			// basic Euler: TODO extend this to include BasicPhysicsEngine.GRAVITY
			//pos.x+=vel.x*DELTA_T;
			//pos.y+=vel.y*DELTA_T;



			pos = pos.addScaled(vel, DELTA_T);
			vel = vel.addScaled(acc, DELTA_T);
		}
	}


	public void draw(Graphics2D g) {
		int x = BasicPhysicsEngine.convertWorldXtoScreenX(pos.x);
		int y = BasicPhysicsEngine.convertWorldYtoScreenY(pos.y);
		g.setColor(col);
		g.fillOval(x - RADIUS_OF_PARTICLE_IN_PIXELS, y - RADIUS_OF_PARTICLE_IN_PIXELS, 2 * RADIUS_OF_PARTICLE_IN_PIXELS, 2 * RADIUS_OF_PARTICLE_IN_PIXELS);
	}

	public double getRadius() {
		return radius;
	}

	public double getX() {
		return pos.x;
	}
	public double getY() {
		return pos.y;
	}
}
