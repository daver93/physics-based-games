package pbgLecture1lab;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.border.Border;


public class ThreadedGuiForPhysicsEngine {
	
	public ThreadedGuiForPhysicsEngine() {
	}

	private static double angle;
	private static double speed;

	public static JLabel xPos;
	public static JLabel yPos;
	public static JLabel xRange;

	public static JLabel exactRangeValue;
	public static JLabel exactMaximumHeightValue;

	private static JButton jButton_go;
	private static Thread theThread;
	public static void main(String[] args) throws Exception {
		BasicPhysicsEngine game = new BasicPhysicsEngine ();
		final BasicView view = new BasicView(game);
		JComponent mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		mainPanel.add(view, BorderLayout.CENTER);

		JPanel sidePanel=new JPanel();
		sidePanel.setLayout(new GridLayout(0, 1));

		JPanel buttonPanel = new JPanel(new FlowLayout());
		sidePanel.add(buttonPanel);


		jButton_go=new JButton("Go");
		buttonPanel.add(jButton_go);
		mainPanel.add(sidePanel, BorderLayout.WEST);
		// add any new buttons or textfields to side panel here...

		JLabel inputAngleLabel = new JLabel("Angle: ");
		JTextField inputAngle = new JTextField("0.0");
		inputAngle.setMinimumSize(new Dimension(100, 100));
		JLabel inputSpeedLabel = new JLabel("Speed: ");
		JTextField inputSpeed = new JTextField("0.0");


		JPanel anglePanel = new JPanel(new FlowLayout());
		anglePanel.add(inputAngleLabel);
		anglePanel.add(inputAngle);
		sidePanel.add(anglePanel);

		JPanel speedPanel = new JPanel(new FlowLayout());
		speedPanel.add(inputSpeedLabel);
		speedPanel.add(inputSpeed);
		sidePanel.add(speedPanel);

		JPanel exactMaximumHeightPanel = new JPanel(new FlowLayout());
		JLabel exactMaximumHeightLabel = new JLabel("Exact Max Height: ");
		exactMaximumHeightValue = new JLabel("");
		exactMaximumHeightPanel.add(exactMaximumHeightLabel);
		exactMaximumHeightPanel.add(exactMaximumHeightValue);
		sidePanel.add(exactMaximumHeightPanel);

		JPanel exactRangePanel = new JPanel(new FlowLayout());
		JLabel exactRangeLabel = new JLabel("Exact Range: ");
		exactRangeValue = new JLabel("");
		exactRangePanel.add(exactRangeLabel);
		exactRangePanel.add(exactRangeValue);
		sidePanel.add(exactRangePanel);


		JPanel exactValuesPanel = new JPanel();
		exactValuesPanel.setLayout(new BoxLayout(exactValuesPanel, BoxLayout.PAGE_AXIS));

		mainPanel.add(exactValuesPanel, BorderLayout.SOUTH);

		JComponent topPanel=new JPanel();
		topPanel.setLayout(new FlowLayout());

		xPos = new JLabel();
		yPos = new JLabel();
		xRange = new JLabel();
		topPanel.add(xPos);
		topPanel.add(yPos);
		topPanel.add(xRange);
		mainPanel.add(topPanel, BorderLayout.NORTH);
		
		new JEasyFrame(mainPanel, "Basic Physics Engine");
		
		ActionListener listener=new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getSource()==jButton_go) {
					try {

						angle = Double.parseDouble(inputAngle.getText());
						speed = Double.parseDouble(inputSpeed.getText());

						double vx = speed * Math.cos(angle/180.0*Math.PI);
						double vy = speed * Math.sin(angle/180.0*Math.PI);

						exactMaximumHeightValue.setText(String.valueOf(exactMaximumHeight(speed, angle)));
						System.out.println("Exact maximum height: " + exactMaximumHeight(speed, angle));
						exactRangeValue.setText(String.valueOf(exactRange(speed, angle)));
						System.out.println("Exact range: " + exactRange(speed, angle));

						// recreate all particles in their original positions:
						final BasicPhysicsEngine game2 = new BasicPhysicsEngine (vx, vy);
						// Tell the view object to start displaying this new Physics engine instead:
						view.updateGame(game2);
						// start a new thread for the new game object:
						startThread(game2, view);
					} catch (InterruptedException e1) {
						e1.printStackTrace();
					}
				}
			}
		};
		jButton_go.addActionListener(listener);
	}
	private static void startThread(final BasicPhysicsEngine game, final BasicView view) throws InterruptedException {
	    Runnable r = new Runnable() {
	         public void run() {
	        	// this while loop will exit any time this method is called for a second time, because
				double xMaxPos = 0;
				double yMaxPos = 0;

				double initialX = -101;



	    		while (theThread==Thread.currentThread()) {
	    			for (int i=0;i<BasicPhysicsEngine.NUM_EULER_UPDATES_PER_SCREEN_REFRESH;i++) {
	    				game.update();
	    			}
    				view.repaint();

	    			if (initialX < -100){
	    				initialX = game.ballPosition.x;
					}

					if (game.ballPosition.x > xMaxPos){
						xMaxPos = game.ballPosition.x;
						xPos.setText(String.valueOf("X: " + game.ballPosition.x));
						xRange.setText(String.valueOf("    Range: From " + initialX + "  To  " + xMaxPos));
					}
					if (game.ballPosition.y > yMaxPos){
						yMaxPos = game.ballPosition.x;

					}
					yPos.setText(String.valueOf("   Y: " + game.ballPosition.y + "     MaxY: " + yMaxPos));
	    			try {
						Thread.sleep(BasicPhysicsEngine.DELAY);
					} catch (InterruptedException e) {
					}
	    		}
	         }
	     };

	     theThread=new Thread(r);// this will cause any old threads running to self-terminate
	     theThread.start();
	}
	

	public static double exactMaximumHeight(double velocity, double angle){
		//Exact maximum Height is = (Velocity^2 * (sin(angle))^2 ) / 2g
		return ( (Math.pow(velocity, 2) * (Math.pow( Math.sin(Math.toRadians(angle)), 2 ) ) )/ ( 2 * BasicPhysicsEngine.GRAVITY) );
	}

	public static double exactRange(double velocity, double angle){
		//Exact Range is = (Velocity^2 * sin2(30)) / g
		return ( (Math.pow(velocity, 2) * 2 * Math.sin(Math.toRadians(angle)) * Math.cos(Math.toRadians(angle))) / BasicPhysicsEngine.GRAVITY );
	}
	

}


